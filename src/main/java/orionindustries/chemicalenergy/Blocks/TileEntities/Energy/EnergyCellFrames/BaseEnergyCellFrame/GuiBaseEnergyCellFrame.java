// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.AdvancedEnergyCellFrame.TEAdvancedEnergyCellFrame;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.TEBasicEnergyCellFrame;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Render.Gui.GuiUtilities;
import orionindustries.chemicalenergy.Utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

public class GuiBaseEnergyCellFrame extends GuiContainer
{
	int xSize = 176;
	int ySize = 183;

	// please set this manually with 'super.tile = this' after calling super(new inventory)
	protected TEBaseEnergyCellFrame tile;

	public GuiBaseEnergyCellFrame(Container cont)
	{
		super(cont);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		// Bind Texture
		if(this.tile instanceof TEBasicEnergyCellFrame)
			this.mc.getTextureManager().bindTexture(Constants.Resources.BasicEnergyCellFrameGui.GetResource());

		else if(this.tile instanceof TEAdvancedEnergyCellFrame)
			this.mc.getTextureManager().bindTexture(Constants.Resources.AdvancedEnergyCellFrameGui.GetResource());

		// set the x for the texture, Total width - textureSize / 2
		par2 = (this.width - this.xSize) / 2;
		// set the y for the texture, Total height - textureSize - 30 (up) / 2,
		int j = (this.height - this.ySize) / 2;
		// draw the texture
		this.drawTexturedModalRect(par2, j, 0, 0, this.xSize, this.ySize);

		double totale = this.tile.GetMaxEnergy() + 0.0;
		double actuale = this.tile.GetStoredEnergy() + 0.0;

		double barheight = 40.0;
		double height = (actuale / totale) * barheight;

		GuiUtilities.RenderEnergyBar(this, par2 + 82, j + 32 + (int) barheight - (int) height, (int) height);
	}

	@Override
	public void drawScreen(int mx, int my, float par3)
	{
		super.drawScreen(mx, my, par3);

		int x = (this.width - this.xSize) / 2;
		int y = (this.height - this.ySize) / 2;

		if(mx >= x + 80 && my >= y + 18 && mx <= x + 80 + 16 && my <= y + 18 + 70)
		{
			List<String> list = new ArrayList<>();

			String dispse = Utilities.FormatNumber(this.tile.GetStoredEnergy(), false);
			String dispme = Utilities.FormatNumber(this.tile.GetMaxEnergy());

			list.add(String.format("%sRF", dispse));
			list.add(String.format("§7%sRF§f", dispme));

			this.drawHoveringText(list, mx, my, this.fontRendererObj);
		}
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
}
