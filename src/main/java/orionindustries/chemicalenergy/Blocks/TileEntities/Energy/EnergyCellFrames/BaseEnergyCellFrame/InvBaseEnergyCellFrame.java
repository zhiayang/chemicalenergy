// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack.ItemBatteryRack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery.GenericBattery;

public abstract class InvBaseEnergyCellFrame extends Container
{
	protected TEBaseEnergyCellFrame tileentity;
	protected InventoryPlayer playerinv;

	private int StoredEnergy;

	private static int INV_US;
	private static int INV_BATTERIES;
	private static int INV_START;
	private static int INV_END;
	private static int HOTBAR_START;
	private static int HOTBAR_END;

	private InvBaseEnergyCellFrame()
	{
	}

	public InvBaseEnergyCellFrame(InventoryPlayer inv, TileEntity te)
	{
		assert(te instanceof TEBaseEnergyCellFrame);
		this.tileentity = (TEBaseEnergyCellFrame)te;
		this.playerinv = inv;
		this.CreateCustomInventorySlots();
		this.CreatePlayerInventorySlots();

		INV_US = 0;
		INV_BATTERIES = INV_US + this.tileentity.getSizeInventory() - INV_US;
		INV_START = INV_BATTERIES + 1;
		INV_END = INV_START + 26;
		HOTBAR_START = INV_END + 1;
		HOTBAR_END = HOTBAR_START + 8;
	}

	protected abstract void CreateCustomInventorySlots();

	protected void CreatePlayerInventorySlots()
	{
		int id = 0;


		for(int i = 0; i < 9; i++)
		{
			this.addSlotToContainer(new Slot(this.playerinv, id, i * 18 + 8, 150)); // Adds player hotbar
			id++;
		}

		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 9; j++)
			{
				this.addSlotToContainer(new Slot(this.playerinv, id, j * 18 + 8, i * 18 + 92)); // Adds player inventory
				id++;
			}
		}
	}

	@Override
	public void addCraftingToCrafters(ICrafting crafting)
	{
		super.addCraftingToCrafters(crafting);

		crafting.sendProgressBarUpdate(this, 0, this.tileentity.GetStoredEnergy());
	}

	@Override
	public void updateProgressBar(int par1, int par2)
	{
		switch(par1)
		{
			case 0:
				this.tileentity.GetEnergyHandler().DirectlyModifyEnergy(par2);
				break;
		}
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		for(Object crafter : this.crafters)
		{
			ICrafting icrafting = (ICrafting) crafter;

			if(this.StoredEnergy != this.tileentity.GetStoredEnergy())
			{
				icrafting.sendProgressBarUpdate(this, 0, this.tileentity.GetStoredEnergy());
				this.StoredEnergy = this.tileentity.GetStoredEnergy();
			}
		}
	}

	@Override
	public boolean canInteractWith(EntityPlayer var1)
	{
		return true;
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slotnum)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(slotnum);

		if(slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if(slotnum < INV_START)
			{
				// try to place in player inventory / action bar
				if(!this.mergeItemStack(itemstack1, INV_START - 2, HOTBAR_END, true))
				{
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			}

			// Item is in inventory / hotbar, try to place ourselves
			else
			{
				if(itemstack1.getItem() instanceof ItemBatteryRack)
				{
					if(!this.mergeItemStack(itemstack1, INV_US + 1, INV_BATTERIES + 1, false))
					{
						return null;
					}
				}
				else if(GenericBattery.IsDryCell(itemstack1))
				{
					if(!this.mergeItemStack(itemstack1, 0, INV_US + 1, false))
						return null;
				}

				// item in player's inventory, but not in action bar
				else if(slotnum >= INV_START && slotnum < HOTBAR_START)
				{
					// place in action bar
					if(!this.mergeItemStack(itemstack1, HOTBAR_START, HOTBAR_START + 1, false))
					{
						return null;
					}
				}
				// item in action bar - place in player inventory
				else if(slotnum >= HOTBAR_START && slotnum < HOTBAR_END + 1)
				{
					if(!this.mergeItemStack(itemstack1, INV_START, INV_END + 1, false))
					{
						return null;
					}
				}
			}

			if(itemstack1.stackSize == 0)
			{
				slot.putStack(null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if(itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
		}

		return itemstack;
	}
}
