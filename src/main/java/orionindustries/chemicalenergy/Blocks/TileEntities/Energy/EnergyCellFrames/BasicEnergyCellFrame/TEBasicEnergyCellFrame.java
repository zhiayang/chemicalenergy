// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame;

import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame.TEBaseEnergyCellFrame;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Energy.StorageEnergyHandler;

public class TEBasicEnergyCellFrame extends TEBaseEnergyCellFrame
{
	public TEBasicEnergyCellFrame()
	{
		super(2);
		this.EnergyStore = new StorageEnergyHandler(Constants.EnergyCellValues.BasicEnergyCellFrame.MaxEnergy(),
				Constants.EnergyCellValues.BasicEnergyCellFrame.MaxBCInput(), this);
		this.BatteryTransferRate = Constants.EnergyCellValues.BasicEnergyCellFrame.BatteryTransferRate();
	}
}
