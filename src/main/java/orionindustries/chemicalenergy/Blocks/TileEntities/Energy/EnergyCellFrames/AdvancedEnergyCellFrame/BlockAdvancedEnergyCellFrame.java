// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.AdvancedEnergyCellFrame;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractContainerBlock;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Utilities.Annotations.RequiresTileEntity;
import orionindustries.chemicalenergy.Utilities.Constants;

@RequiresTileEntity(TileEntityClass = TEAdvancedEnergyCellFrame.class, ContainerClass =  InvAdvancedEnergyCellFrame.class,
	GuiContainerClass = GuiAdvancedEnergyCellFrame.class, GuiID = 1)
public class BlockAdvancedEnergyCellFrame extends AbstractContainerBlock
{
	public BlockAdvancedEnergyCellFrame()
	{
		super(Material.rock);
		this.setHardness(1.0f);
		this.setResistance(1.0f);
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
		this.setBlockName(Constants.BlockNames.AdvancedEnergyCellFrame.toString());
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata)
	{
		return new TEAdvancedEnergyCellFrame();
	}

	@Override
	public boolean hasTileEntity(int metadata)
	{
		return true;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer p, int p1, float p2, float p3, float p4)
	{
		p.openGui(ChemicalEnergy.Instance, Constants.GuiIDs.AdvancedEnergyCellFrame.getID(), world, x, y, z);
		return true;
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
	{
		assert (world.getTileEntity(x, y, z) instanceof TEAdvancedEnergyCellFrame);
		((TEAdvancedEnergyCellFrame) (world.getTileEntity(x, y, z))).DropItems();
		super.breakBlock(world, x, y, z, block, meta);
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2)
	{
		return new TEAdvancedEnergyCellFrame();
	}


	@Override
	public boolean renderAsNormalBlock()
	{
		return true;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return true;
	}

//	@Override
//	public int getRenderType()
//	{
//		return -1;
//	}
}
