// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.Renderers.BasicEnergyCellFrameISBRH;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractContainerBlock;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Utilities.Annotations.RequiresTileEntity;
import orionindustries.chemicalenergy.Utilities.Constants;

@RequiresTileEntity(TileEntityClass = TEBasicEnergyCellFrame.class, ContainerClass =  InvBasicEnergyCellFrame.class,
	GuiContainerClass = GuiBasicEnergyCellFrame.class, GuiID = 0)
public class BlockBasicEnergyCellFrame extends AbstractContainerBlock
{
	public BlockBasicEnergyCellFrame()
	{
		super(Material.rock);
		this.setHardness(1.0f);
		this.setResistance(1.0f);
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
		this.setBlockName(Constants.BlockNames.BasicEnergyCellFrame.toString());
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata)
	{
		return new TEBasicEnergyCellFrame();
	}

	@Override
	public boolean hasTileEntity(int metadata)
	{
		return true;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer p, int p1, float p2, float p3, float p4)
	{
		p.openGui(ChemicalEnergy.Instance, Constants.GuiIDs.BasicEnergyCellFrame.getID(), world, x, y, z);
		return true;
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
	{
		assert (world.getTileEntity(x, y, z) instanceof TEBasicEnergyCellFrame);
		((TEBasicEnergyCellFrame) (world.getTileEntity(x, y, z))).DropItems();
		super.breakBlock(world, x, y, z, block, meta);
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2)
	{
		return new TEBasicEnergyCellFrame();
	}

	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@Override
	public int getRenderType()
	{
		return BasicEnergyCellFrameISBRH.RenderID;
	}
}
