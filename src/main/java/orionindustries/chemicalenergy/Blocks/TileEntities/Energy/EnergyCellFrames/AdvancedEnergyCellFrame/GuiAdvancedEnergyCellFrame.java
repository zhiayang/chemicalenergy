// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.AdvancedEnergyCellFrame;

import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.tileentity.TileEntity;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame.GuiBaseEnergyCellFrame;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame.TEBaseEnergyCellFrame;

public class GuiAdvancedEnergyCellFrame extends GuiBaseEnergyCellFrame
{
	public GuiAdvancedEnergyCellFrame(InventoryPlayer inv, TileEntity te)
	{
		super(new InvAdvancedEnergyCellFrame(inv, te));
		super.tile = (TEBaseEnergyCellFrame)te;
		this.allowUserInput = true;
	}
}
