// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.Renderers;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import org.lwjgl.opengl.GL11;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.TEBasicEnergyCellFrame;
import orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack.ItemBatteryRack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.LeadAcidBattery;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Render.Model.WavefrontObj.Object3D;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BasicEnergyCellFrameTESR extends TileEntitySpecialRenderer
{
	//	private IModelCustom model;
	private Object3D model;
	private Object3D WetCellModel;
	private Object3D DryCellRackModel;
	private Object3D DryCellModel;

	public static BasicEnergyCellFrameTESR Instance;

	public BasicEnergyCellFrameTESR()
	{
		try
		{
			this.model = new Object3D(new BufferedReader(new InputStreamReader(Minecraft.getMinecraft().getResourceManager()
				.getResource(Constants.Resources.BasicEnergyCellFrameModel.GetResource()).getInputStream())), true);

			this.WetCellModel = new Object3D(new BufferedReader(new InputStreamReader(Minecraft.getMinecraft().getResourceManager()
				.getResource(Constants.Resources.WetCellModel.GetResource()).getInputStream())), true);

			this.DryCellRackModel = new Object3D(new BufferedReader(new InputStreamReader(Minecraft.getMinecraft().getResourceManager()
				.getResource(Constants.Resources.DryCellRackModel.GetResource()).getInputStream())), true);

			this.DryCellModel = new Object3D(new BufferedReader(new InputStreamReader(Minecraft.getMinecraft().getResourceManager()
				.getResource(Constants.Resources.DryCellModel.GetResource()).getInputStream())), true);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		Instance = this;
	}

	@Override
	public void renderTileEntityAt(TileEntity te, double x, double y, double z, float var8)
	{
		GL11.glPushMatrix();
		FMLClientHandler.instance().getClient().getTextureManager()
			.bindTexture(Constants.Resources.BasicEnergyCellFrameTexture.GetResource());

		GL11.glTranslated(x + 0.5, y + 0.5, z + 0.5);
		GL11.glScaled(0.01, 0.01, 0.01);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glColor4d(1.0, 1.0, 1.0, 1.0);

		this.model.Render();

		assert (te instanceof TEBasicEnergyCellFrame);
		TEBasicEnergyCellFrame frame = (TEBasicEnergyCellFrame) te;
		if(frame.GetInventory()[1] != null)
		{
			if(frame.GetInventory()[1].getItem() instanceof LeadAcidBattery)
			{
				FMLClientHandler.instance().getClient().getTextureManager()
					.bindTexture(Constants.Resources.WetCellTexture.GetResource());

				this.WetCellModel.Render();
			}
			else if(frame.GetInventory()[1].getItem() instanceof ItemBatteryRack)
			{
				FMLClientHandler.instance().getClient().getTextureManager()
					.bindTexture(Constants.Resources.DryCellRackTexture.GetResource());

				this.DryCellRackModel.Render();

				// now render the cells.

				FMLClientHandler.instance().getClient().getTextureManager()
					.bindTexture(Constants.Resources.DryCellTexture.GetResource());


				ItemStack[] rack = ItemBatteryRack.GetRackContents(frame.GetInventory()[1]);
				GL11.glTranslated(-30, 0, 20);
				GL11.glScaled(1, 0.9, 1);

				for(int i = 0; i < rack.length; i++)
				{
					if(rack[i] != null)
					{
						this.DryCellModel.Render();
					}

					GL11.glTranslated(30, 0, 0);

					if(i == 2)
						GL11.glTranslated(-90, 0, -40);
				}
			}
		}


		GL11.glPopMatrix();
	}
}
