// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame;

import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerHandler;
import cofh.api.energy.IEnergyHandler;
import cofh.api.tileentity.IEnergyInfo;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractTileEntityContainer;
import orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack.ItemBatteryRack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery.GenericBattery;
import orionindustries.chemicalenergy.Items.Energy.Batteries.LeadAcidBattery;
import orionindustries.chemicalenergy.Utilities.Energy.GenericEnergyHandler;
import orionindustries.chemicalenergy.Utilities.Energy.ITileCapacitor;
import orionindustries.chemicalenergy.Utilities.Energy.StorageEnergyHandler;

import java.util.ArrayList;
import java.util.List;

public abstract class TEBaseEnergyCellFrame extends AbstractTileEntityContainer
	implements IEnergyHandler, IPowerReceptor, ITileCapacitor, IEnergyInfo
{
	protected StorageEnergyHandler EnergyStore;
	protected int BatteryTransferRate;

	protected TEBaseEnergyCellFrame(int invsize)
	{
		super(invsize);
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		this.EnergyStore.WriteToNBT(tag);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		this.EnergyStore.ReadFromNBT(tag);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
	{
		super.onDataPacket(net, pkt);
		this.EnergyStore.ReadFromNBT(pkt.func_148857_g());
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound tag = new NBTTagCompound();
		this.writeToNBT(tag);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, tag);
	}



	private ItemStack[] GetEnergyUnits()
	{
		List<ItemStack> stacks = new ArrayList<>();

		for(int i = 1; i < this.getSizeInventory(); i++)
			if(this.GetInventory()[i] != null)
				if(this.GetInventory()[i].getItem() instanceof LeadAcidBattery || this.GetInventory()[i].getItem() instanceof ItemBatteryRack)
					stacks.add(this.GetInventory()[i]);



		return stacks.toArray(new ItemStack[stacks.size()]);
	}

	@Override
	public void updateEntity()
	{
		// check for this->batt
		if(this.GetInventory()[0] != null)
		{
			ItemStack stack = this.GetInventory()[0];
			assert (stack.getItem() instanceof GenericBattery);

			GenericBattery battery = (GenericBattery) stack.getItem();
			int extracted = this.ExtractEnergy(this.BatteryTransferRate);
			int stored = battery.StoreEnergy(stack, extracted);

			this.StoreEnergy(extracted - stored);
		}

		this.OutputEnergy();
	}

	private void OutputEnergy()
	{
		// right now, output everywhere and input everywhere.
		if(this.GetStoredEnergy() == 0)
			return;

		for(ForgeDirection f : ForgeDirection.VALID_DIRECTIONS)
		{
			TileEntity te = this.worldObj.getTileEntity(this.xCoord + f.offsetX, this.yCoord + f.offsetY, this.zCoord + f.offsetZ);
			if(te instanceof IEnergyHandler)
			{
				IEnergyHandler ieh = (IEnergyHandler) te;
				this.ExtractEnergy(ieh.receiveEnergy(f, this.BatteryTransferRate, false));
			}
		}
	}

	@Override
	public void markDirty()
	{
		// re-read the contents inside our slots.
		int newCapacity = 0;
		int newEnergy = 0;
		int newTransfer = 0;

		for(ItemStack stack : this.GetEnergyUnits())
		{
			if(stack.getItem() instanceof ItemBatteryRack)
			{
				newCapacity += ItemBatteryRack.Instance.GetMaxEnergy(stack);
				newEnergy += ItemBatteryRack.Instance.GetStoredEnergy(stack);
				newTransfer += ItemBatteryRack.Instance.GetTransferRate(stack);
			}
			else if(stack.getItem() instanceof LeadAcidBattery)
			{
				newCapacity += ((LeadAcidBattery) stack.getItem()).GetMaxEnergy(stack);
				newEnergy += ((LeadAcidBattery) stack.getItem()).GetStoredEnergy(stack);
				newTransfer += ((LeadAcidBattery) stack.getItem()).GetTransferRate(stack);
			}
		}

		this.EnergyStore.DirectlyModifyEnergy(newEnergy);
		this.EnergyStore.SetMaxEnergy(newCapacity);
		this.EnergyStore.SetTransferRate(newTransfer);
		this.BatteryTransferRate = this.EnergyStore.GetTransferRate();
		this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);

	}










	@Override
	public GenericEnergyHandler GetEnergyHandler()
	{
		return this.EnergyStore;
	}

	@Override
	public int GetStoredEnergy()
	{
		int stored = 0;
		for(ItemStack stack : this.GetEnergyUnits())
		{
			if(stack.getItem() instanceof ItemBatteryRack)
			{
				stored += ItemBatteryRack.Instance.GetStoredEnergy(stack);
			}
			else if(stack.getItem() instanceof LeadAcidBattery)
			{
				stored += ((LeadAcidBattery) stack.getItem()).GetStoredEnergy(stack);
			}
		}
		this.EnergyStore.DirectlyModifyEnergy(stored);

		return this.EnergyStore.GetStoredEnergy();
	}

	@Override
	public int GetMaxEnergy()
	{
		int stored = 0;
		for(ItemStack stack : this.GetEnergyUnits())
		{
			if(stack.getItem() instanceof ItemBatteryRack)
			{
				stored += ItemBatteryRack.Instance.GetMaxEnergy(stack);
			}
			else if(stack.getItem() instanceof LeadAcidBattery)
			{
				stored += ((LeadAcidBattery) stack.getItem()).GetMaxEnergy(stack);
			}
		}
		this.EnergyStore.SetMaxEnergy(stored);

		return this.EnergyStore.GetMaxEnergy();
	}

	@Override
	public int StoreEnergy(int Energy)
	{
		// make these also update the cell racks.
		int num = this.GetEnergyUnits().length;

		if(num == 0)
			return 0;

		int avgEnergy = Energy / num;
		for(ItemStack stack : this.GetEnergyUnits())
		{
			int remaining = 0;
			if(stack.getItem() instanceof ItemBatteryRack)
			{
				remaining = avgEnergy - ItemBatteryRack.Instance.StoreEnergy(stack, avgEnergy);
			}
			else if(stack.getItem() instanceof LeadAcidBattery)
			{
				int stored = ((LeadAcidBattery) stack.getItem()).StoreEnergy(stack, avgEnergy);
				remaining = avgEnergy - stored;
			}
			avgEnergy += remaining / num;
		}

		return this.EnergyStore.StoreEnergy(Energy);
	}

	@Override
	public int ExtractEnergy(int Energy)
	{
		// make these update the cell racks as well.
		int num = this.GetEnergyUnits().length;

		if(num == 0)
			return 0;

		int avgEnergy = Energy / num;
		for(ItemStack stack : this.GetEnergyUnits())
		{
			int remaining = 0;
			if(stack.getItem() instanceof ItemBatteryRack)
			{
				remaining = avgEnergy - ItemBatteryRack.Instance.ExtractEnergy(stack, avgEnergy);
			}
			else if(stack.getItem() instanceof LeadAcidBattery)
			{
				remaining = avgEnergy - ((LeadAcidBattery) stack.getItem()).ExtractEnergy(stack, avgEnergy);
			}
			avgEnergy += remaining / num;
		}

		return this.EnergyStore.ExtractEnergy(Energy);
	}


	@Override
	public int receiveEnergy(ForgeDirection from, int maxReceive, boolean simulate)
	{
		if(!simulate)
		{
			return this.StoreEnergy(maxReceive);
		}

		return 0;
	}

	@Override
	public int extractEnergy(ForgeDirection from, int maxExtract, boolean simulate)
	{
		if(!simulate)
		{
			return this.ExtractEnergy(maxExtract);
		}

		return 0;
	}

	@Override
	public boolean canInterface(ForgeDirection from)
	{
		return true;
	}

	@Override
	public int getEnergyStored(ForgeDirection from)
	{
		return this.GetStoredEnergy();
	}

	@Override
	public int getMaxEnergyStored(ForgeDirection from)
	{
		return this.GetMaxEnergy();
	}



	@Override
	public void closeInventory()
	{
//		this.StoreEnergy(0);
	}

	@Override
	public void openInventory()
	{
//		this.ExtractEnergy(0);
	}

	@Override
	public int getEnergyPerTick()
	{
		return this.BatteryTransferRate;
	}

	@Override
	public int getMaxEnergyPerTick()
	{
		return this.BatteryTransferRate;
	}

	@Override
	public int getEnergy()
	{
		return this.GetStoredEnergy();
	}

	@Override
	public int getMaxEnergy()
	{
		return this.GetMaxEnergy();
	}

	@Override
	public PowerHandler.PowerReceiver getPowerReceiver(ForgeDirection side)
	{
		return this.EnergyStore.GetBCPowerHandler();
	}

	@Override
	public void doWork(PowerHandler workProvider)
	{
		this.EnergyStore.BCDoWork();
	}

	@Override
	public World getWorld()
	{
		return this.worldObj;
	}
}
