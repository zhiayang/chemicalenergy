// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame;

import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.tileentity.TileEntity;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame.InvBaseEnergyCellFrame;
import orionindustries.chemicalenergy.Utilities.Slots.FEBatterySlot;
import orionindustries.chemicalenergy.Utilities.Slots.FEEnergyDeviceSlot;

public class InvBasicEnergyCellFrame extends InvBaseEnergyCellFrame
{
	public InvBasicEnergyCellFrame(InventoryPlayer inv, TileEntity te)
	{
		super(inv, te);
	}

	@Override
	protected void CreateCustomInventorySlots()
	{
		this.addSlotToContainer(new FEBatterySlot(this.tileentity, 0, 43, 35));
		this.addSlotToContainer(new FEEnergyDeviceSlot(this.tileentity, 1, 117, 35));
	}
}
