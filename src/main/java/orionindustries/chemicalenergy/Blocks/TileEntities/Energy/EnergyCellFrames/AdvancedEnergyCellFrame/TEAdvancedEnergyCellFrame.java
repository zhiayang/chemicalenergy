// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.AdvancedEnergyCellFrame;

import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BaseEnergyCellFrame.TEBaseEnergyCellFrame;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Energy.StorageEnergyHandler;

public class TEAdvancedEnergyCellFrame extends TEBaseEnergyCellFrame
{
	public TEAdvancedEnergyCellFrame()
	{
		super(3);
		this.EnergyStore = new StorageEnergyHandler(Constants.EnergyCellValues.AdvancedEnergyCellFrame.MaxEnergy(),
				Constants.EnergyCellValues.AdvancedEnergyCellFrame.MaxBCInput(), this);
		this.BatteryTransferRate = Constants.EnergyCellValues.BasicEnergyCellFrame.BatteryTransferRate();
	}
}
