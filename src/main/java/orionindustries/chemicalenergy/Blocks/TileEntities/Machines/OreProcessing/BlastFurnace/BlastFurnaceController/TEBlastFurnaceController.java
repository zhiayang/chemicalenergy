// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.BlastFurnace.BlastFurnaceController;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock.AbstractMBTileController;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.MachineRecipe;

public class TEBlastFurnaceController extends AbstractMBTileController
{
	private int HeatLevel = 0;
	private final int MaxHeatLevel = Constants.OtherValues.BlastFurnaceMaxHeatLevel.Value();

	private int Progress1 = 0;
	private int Progress2 = 0;
	private int Progress3 = 0;
	private int Progress4 = 0;

	private double MaxCatalyst = 5000.0;
	private double RemainingCatalyst = 0;
	private Constants.CatalystTypes catalysttype;


	ItemStack CachedP1Output;
	ItemStack CachedP2Output;
	ItemStack CachedP3Output;
	ItemStack CachedP4Output;

	ItemStack CachedP1Input;
	ItemStack CachedP2Input;
	ItemStack CachedP3Input;
	ItemStack CachedP4Input;

	MachineRecipe CachedP1Recipe;
	MachineRecipe CachedP2Recipe;
	MachineRecipe CachedP3Recipe;
	MachineRecipe CachedP4Recipe;


	public TEBlastFurnaceController()
	{
		super(9);
		this.Progress1 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
		this.Progress2 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
		this.Progress3 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
		this.Progress4 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
	}

	public double getRemainingCatalyst()
	{
		return this.RemainingCatalyst;
	}

	public int getProgress1()
	{
		return this.Progress1;
	}

	public int getProgress2()
	{
		return this.Progress2;
	}

	public int getProgress3()
	{
		return this.Progress3;
	}

	public int getProgress4()
	{
		return this.Progress4;
	}

	public void setProgress1(int progress1)
	{
		this.Progress1 = progress1;
	}

	public void setProgress2(int progress2)
	{
		this.Progress2 = progress2;
	}

	public void setProgress3(int progress3)
	{
		this.Progress3 = progress3;
	}

	public void setProgress4(int progress4)
	{
		this.Progress4 = progress4;
	}


	@Override
	public void updateEntity()
	{
		if(!this.worldObj.isRemote)
		{
			if(this.HeatLevel < this.MaxHeatLevel)
				this.HeatLevel++;


			if(this.Progress1 >= 0 && this.Inventory[0] == null)
				this.Progress1 = Constants.OtherValues.BlastFurnaceProcessTime.Value();

			if(this.Progress2 >= 0 && this.Inventory[1] == null)
				this.Progress2 = Constants.OtherValues.BlastFurnaceProcessTime.Value();

			if(this.Progress3 >= 0 && this.Inventory[2] == null)
				this.Progress3 = Constants.OtherValues.BlastFurnaceProcessTime.Value();

			if(this.Progress4 >= 0 && this.Inventory[3] == null)
				this.Progress4 = Constants.OtherValues.BlastFurnaceProcessTime.Value();


			if(this.Inventory[8] != null)
			{
				if(Constants.CarbonItems.containsKey(this.Inventory[8].getItem()))
				{
					int val = Constants.CarbonItems.get(this.Inventory[8].getItem());

					if(this.RemainingCatalyst + val < this.MaxCatalyst)
					{
						this.catalysttype = Constants.CatalystTypes.Carbon;
						this.RemainingCatalyst += val;
						this.DecreaseStack(8, 1);
					}
				}
			}



			if(this.CachedP1Output != null)
			{
				int size = this.CachedP1Output.stackSize * this.CachedP1Recipe.GetWeightedMultiplier();
				if(this.Progress1 == 0)
				{
					boolean s = this.IncreaseStack(4, this.CachedP1Output, size);

					if(s)
					{
						this.DecreaseStack(0, this.CachedP1Input.stackSize);
					}

					this.Progress1 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				}

				else if(this.Progress1 > 0 && this.Inventory[0] != null && this.CanCombineItems(this.CachedP1Output, this.Inventory[4])
					&& (this.Inventory[4] == null || this.Inventory[4].stackSize + size <= this.Inventory[4].getMaxStackSize())
					&& this.RemainingCatalyst >= (this.CachedP1Recipe.GetCatalystConsumption() / 20.0) * this.Progress1
					&& this.catalysttype == this.CachedP1Recipe.GetCatalystType())
				{
					this.Progress1--;
					this.RemainingCatalyst -= this.CachedP1Recipe.GetCatalystConsumption() / 20.0;
				}
			}




			if(this.CachedP2Output != null)
			{
				int size = this.CachedP2Output.stackSize * this.CachedP2Recipe.GetWeightedMultiplier();
				if(this.Progress2 == 0)
				{
					boolean s = this.IncreaseStack(5, this.CachedP2Output, size);

					if(s)
						this.DecreaseStack(1, this.CachedP2Input.stackSize);

					this.Progress2 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				}

				else if(this.Progress2 > 0 && this.Inventory[1] != null && this.CanCombineItems(this.CachedP2Output, this.Inventory[5])
					&& (this.Inventory[5] == null || this.Inventory[5].stackSize + size <= this.Inventory[5].getMaxStackSize())
					&& this.RemainingCatalyst >= (this.CachedP2Recipe.GetCatalystConsumption() / 20.0) * this.Progress2
					&& this.catalysttype == this.CachedP2Recipe.GetCatalystType())
				{
					this.Progress2--;
					this.RemainingCatalyst -= this.CachedP2Recipe.GetCatalystConsumption() / 20.0;
				}
			}





			if(this.CachedP3Output != null)
			{
				int size = this.CachedP3Output.stackSize * this.CachedP3Recipe.GetWeightedMultiplier();
				if(this.Progress3 == 0)
				{
					boolean s = this.IncreaseStack(6, this.CachedP3Output, size);

					if(s)
						this.DecreaseStack(2, this.CachedP3Input.stackSize);

					this.Progress3 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				}

				else if(this.Progress3 > 0 && this.Inventory[2] != null && this.CanCombineItems(this.CachedP3Output, this.Inventory[6])
					&& (this.Inventory[6] == null || this.Inventory[6].stackSize + size <= this.Inventory[6].getMaxStackSize())
					&& this.RemainingCatalyst >= (this.CachedP3Recipe.GetCatalystConsumption() / 20.0) * this.Progress3
					&& this.catalysttype == this.CachedP3Recipe.GetCatalystType())
				{
					this.Progress3--;
					this.RemainingCatalyst -= this.CachedP3Recipe.GetCatalystConsumption() / 20.0;
				}
			}






			if(this.CachedP4Output != null)
			{
				int size = this.CachedP4Output.stackSize * this.CachedP4Recipe.GetWeightedMultiplier();
				if(this.Progress4 == 0)
				{
					boolean s = this.IncreaseStack(7, this.CachedP4Output, size);

					if(s)
						this.DecreaseStack(3, this.CachedP4Input.stackSize);

					this.Progress4 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				}

				else if(this.Progress4 > 0 && this.Inventory[3] != null && this.CanCombineItems(this.CachedP4Output, this.Inventory[7])
					&& (this.Inventory[7] == null || this.Inventory[7].stackSize + size <= this.Inventory[7].getMaxStackSize())
					&& this.RemainingCatalyst >= (this.CachedP4Recipe.GetCatalystConsumption() / 20.0) * this.Progress4
					&& this.catalysttype == this.CachedP4Recipe.GetCatalystType())
				{
					this.Progress4--;
					this.RemainingCatalyst -= this.CachedP4Recipe.GetCatalystConsumption() / 20.0;
				}
			}










			if(this.Inventory[0] != null && this.CanProcess(0) != null && this.CachedP1Output == null)
			{
				this.Progress1 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				this.CachedP1Recipe = this.CanProcess(0);
				this.CachedP1Output = this.CachedP1Recipe.GetOutputs().get(0);
				this.CachedP1Input = this.CachedP1Recipe.GetInputs().get(0);
			}
			else if(this.Progress1 == Constants.OtherValues.BlastFurnaceProcessTime.Value())
				this.CachedP1Output = null;


			if(this.Inventory[1] != null && this.CanProcess(1) != null && this.CachedP2Output == null)
			{
				this.Progress2 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				this.CachedP2Recipe = this.CanProcess(1);
				this.CachedP2Output = this.CachedP2Recipe.GetOutputs().get(0);
				this.CachedP2Input = this.CachedP2Recipe.GetInputs().get(0);
			}
			else if(this.Progress2 == Constants.OtherValues.BlastFurnaceProcessTime.Value())
				this.CachedP2Output = null;


			if(this.Inventory[2] != null && this.CanProcess(2) != null && this.CachedP3Output == null)
			{
				this.Progress3 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				this.CachedP3Recipe = this.CanProcess(2);
				this.CachedP3Output = this.CachedP3Recipe.GetOutputs().get(0);
				this.CachedP3Input = this.CachedP3Recipe.GetInputs().get(0);

			}
			else if(this.Progress3 == Constants.OtherValues.BlastFurnaceProcessTime.Value())
				this.CachedP3Output = null;


			if(this.Inventory[3] != null && this.CanProcess(3) != null && this.CachedP4Output == null)
			{
				this.Progress4 = Constants.OtherValues.BlastFurnaceProcessTime.Value();
				this.CachedP4Recipe = this.CanProcess(3);
				this.CachedP4Output = this.CachedP4Recipe.GetOutputs().get(0);
				this.CachedP4Input = this.CachedP4Recipe.GetInputs().get(0);
			}
			else if(this.Progress4 == Constants.OtherValues.BlastFurnaceProcessTime.Value())
				this.CachedP4Output = null;


			this.worldObj.markBlockForUpdate(this.xCoord, this.yCoord, this.zCoord);
		}
	}


	private MachineRecipe CanProcess(int slot)
	{
		ItemStack stack = this.Inventory[slot];
		if(stack == null)
			return null;

		return MachineRecipe.GetRecipe(CentralRegistry.BlastFurnaceRecipes, stack);
	}


	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound tag = new NBTTagCompound();
		this.writeToNBT(tag);
		tag.setInteger("HeatLevel", this.HeatLevel);
		tag.setInteger("Progress1", this.Progress1);
		tag.setInteger("Progress2", this.Progress2);
		tag.setInteger("Progress3", this.Progress3);
		tag.setInteger("Progress4", this.Progress4);
		tag.setDouble("RemainingCatalyst", this.RemainingCatalyst);
		tag.setInteger("CatalystType", this.catalysttype.ordinal());

		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, tag);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
	{
		super.onDataPacket(net, pkt);
		this.Progress1 = pkt.func_148857_g().getInteger("Progress1");
		this.Progress2 = pkt.func_148857_g().getInteger("Progress2");
		this.Progress3 = pkt.func_148857_g().getInteger("Progress3");
		this.Progress4 = pkt.func_148857_g().getInteger("Progress4");
		this.HeatLevel = pkt.func_148857_g().getInteger("HeatLevel");
		this.RemainingCatalyst = pkt.func_148857_g().getDouble("RemainingCatalyst");
		this.catalysttype = Constants.CatalystTypes.values()[pkt.func_148857_g().getInteger("CatalystType")];
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		this.HeatLevel = tag.getInteger("HeatLevel");
		this.RemainingCatalyst = tag.getDouble("RemainingCatalyst");
		this.catalysttype = Constants.CatalystTypes.values()[tag.getInteger("CatalystType")];
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		tag.setInteger("HeatLevel", this.HeatLevel);
		tag.setDouble("RemainingCatalyst", this.RemainingCatalyst);
		tag.setInteger("CatalystType", this.catalysttype.ordinal());
	}

	public int GetHeatLevel()
	{
		return this.HeatLevel;
	}

	public void SetHeatLevel(int heatLevel)
	{
		this.HeatLevel = heatLevel;
	}

	public int GetMaxHeatLevel()
	{
		return this.MaxHeatLevel;
	}
}
