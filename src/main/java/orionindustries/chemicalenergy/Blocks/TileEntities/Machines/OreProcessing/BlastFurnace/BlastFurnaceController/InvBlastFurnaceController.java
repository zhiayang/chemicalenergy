// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.BlastFurnace.BlastFurnaceController;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import orionindustries.chemicalenergy.Utilities.Slots.FEOutputSlot;
import orionindustries.chemicalenergy.Utilities.Slots.FESlot;

public class InvBlastFurnaceController extends Container
{
	protected TEBlastFurnaceController tileentity;
	protected InventoryPlayer playerinv;

	private int HeatLevel;
	private int Progress1;
	private int Progress2;
	private int Progress3;
	private int Progress4;

	public InvBlastFurnaceController(InventoryPlayer inv, TileEntity te)
	{
		assert (te instanceof TEBlastFurnaceController);

		this.tileentity = (TEBlastFurnaceController) te;
		this.playerinv = inv;
		this.CreatePlayerInventorySlots();
		this.CreateCustomInventorySlots();

	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int par2)
	{
		return null;
	}

	protected void CreatePlayerInventorySlots()
	{
		int id = 0;

		for(int i = 0; i < 9; i++)
		{
			this.addSlotToContainer(new Slot(this.playerinv, id, i * 18 + 8, 158)); // Adds player hotbar
			id++;
		}
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 9; j++)
			{
				this.addSlotToContainer(new Slot(this.playerinv, id, j * 18 + 8, i * 18 + 100)); // Adds player inventory
				id++;
			}
		}
	}

	@Override
	public void addCraftingToCrafters(ICrafting crafting)
	{
		super.addCraftingToCrafters(crafting);

		crafting.sendProgressBarUpdate(this, 0, this.tileentity.GetHeatLevel());
		crafting.sendProgressBarUpdate(this, 1, this.tileentity.getProgress1());
		crafting.sendProgressBarUpdate(this, 2, this.tileentity.getProgress2());
		crafting.sendProgressBarUpdate(this, 3, this.tileentity.getProgress3());
		crafting.sendProgressBarUpdate(this, 4, this.tileentity.getProgress4());
	}

	@Override
	public void updateProgressBar(int ind, int value)
	{
		super.updateProgressBar(ind, value);

		switch(ind)
		{
			case 0:
				this.tileentity.SetHeatLevel(value);
				break;

			case 1:
				this.tileentity.setProgress1(value);
				break;

			case 2:
				this.tileentity.setProgress2(value);
				break;

			case 3:
				this.tileentity.setProgress3(value);
				break;

			case 4:
				this.tileentity.setProgress4(value);
				break;
		}
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();

		for(Object crafter : this.crafters)
		{
			ICrafting icrafting = (ICrafting) crafter;

			if(this.HeatLevel != this.tileentity.GetHeatLevel())
			{
				icrafting.sendProgressBarUpdate(this, 0, this.tileentity.GetHeatLevel());
				this.HeatLevel = this.tileentity.GetHeatLevel();
			}

			if(this.Progress1 != this.tileentity.getProgress1())
			{
				icrafting.sendProgressBarUpdate(this, 1, this.tileentity.getProgress1());
				this.Progress1 = this.tileentity.getProgress1();
			}

			if(this.Progress2 != this.tileentity.getProgress2())
			{
				icrafting.sendProgressBarUpdate(this, 2, this.tileentity.getProgress2());
				this.Progress2 = this.tileentity.getProgress2();
			}

			if(this.Progress3 != this.tileentity.getProgress3())
			{
				icrafting.sendProgressBarUpdate(this, 3, this.tileentity.getProgress3());
				this.Progress3 = this.tileentity.getProgress3();
			}

			if(this.Progress4 != this.tileentity.getProgress4())
			{
				icrafting.sendProgressBarUpdate(this, 4, this.tileentity.getProgress4());
				this.Progress4 = this.tileentity.getProgress4();
			}
		}
	}

	protected void CreateCustomInventorySlots()
	{
		this.addSlotToContainer(new FESlot(this.tileentity, 0, 43, 0));
		this.addSlotToContainer(new FESlot(this.tileentity, 1, 43, 18));
		this.addSlotToContainer(new FESlot(this.tileentity, 2, 43, 36));
		this.addSlotToContainer(new FESlot(this.tileentity, 3, 43, 54));
		this.addSlotToContainer(new FEOutputSlot(this.tileentity, 4, 117, 0));
		this.addSlotToContainer(new FEOutputSlot(this.tileentity, 5, 117, 18));
		this.addSlotToContainer(new FEOutputSlot(this.tileentity, 6, 117, 36));
		this.addSlotToContainer(new FEOutputSlot(this.tileentity, 7, 117, 54));

		this.addSlotToContainer(new FESlot(this.tileentity, 8, 80, 77));
	}

	@Override
	public boolean canInteractWith(EntityPlayer var1)
	{
		return true;
	}
}
