// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherCasing;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherController.TEOreCrusherController;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock.AbstractMBBlockCasing;
import orionindustries.chemicalenergy.Utilities.Annotations.RequiresTileEntity;
import orionindustries.chemicalenergy.Utilities.Constants;

@RequiresTileEntity(TileEntityClass = TEOreCrusherCasing.class, GuiID = 0)
public class BlockOreCrusherCasing extends AbstractMBBlockCasing
{
	IIcon[] icons;

	public BlockOreCrusherCasing()
	{
		super(TEOreCrusherController.class, TEOreCrusherCasing.class);
		this.setHardness(2.0f);
		this.setHarvestLevel("pickaxe", 2);
		this.setBlockName(Constants.BlockNames.OreCrusherCasing.toString());
	}


	@Override
	public IIcon getIcon(int p_149691_1_, int p_149691_2_)
	{
		// called for when in inventory, supposedly.
		return this.icons[0];
	}

	@Override
	public void registerBlockIcons(IIconRegister iir)
	{
		this.icons = new IIcon[9];

		for(int i = 0; i < this.icons.length; i++)
		{
			this.icons[i] = iir.registerIcon(Constants.ResourcePrefix + "Machines/OreCrusher/OreCrusherCasing" + Integer.toString(i + 1));
		}
	}


	@Override
	public boolean hasTileEntity(int metadata)
	{
		return true;
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata)
	{
		return new TEOreCrusherCasing();
	}
}
