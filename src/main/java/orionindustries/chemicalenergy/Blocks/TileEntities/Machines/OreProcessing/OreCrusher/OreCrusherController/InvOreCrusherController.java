// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherController;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;

public class InvOreCrusherController extends Container
{
	@Override
	public boolean canInteractWith(EntityPlayer var1)
	{
		return false;
	}
}
