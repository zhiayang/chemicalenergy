// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherController;

import orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock.AbstractMBTileController;

public class TEOreCrusherController extends AbstractMBTileController
{
	protected TEOreCrusherController()
	{
		super(4);
	}
}
