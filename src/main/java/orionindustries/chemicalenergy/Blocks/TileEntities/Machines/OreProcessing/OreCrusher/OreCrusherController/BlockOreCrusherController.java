// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherController;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherCasing.TEOreCrusherCasing;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock.AbstractMBBlockController;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Utilities.Annotations.RequiresTileEntity;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.MultiblockStructureDefinition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiresTileEntity(TileEntityClass = TEOreCrusherController.class, ContainerClass = InvOreCrusherController.class,
	GuiContainerClass = GuiOreCrusherController.class, GuiID = 5)
public class BlockOreCrusherController extends AbstractMBBlockController
{
	String[][] struct =
		{
			{"aaa", "aaa", "aaa"},
			{"aba", "aca", "aaa"},
			{"aaa", "aca", "aaa"},
			{"aaa", "aca", "aaa"},
			{"aaa", "aaa", "aaa"}
		};

	Map<Character, List<Block>> map = new HashMap<>();

	ArrayList<Block> a1 = new ArrayList<>();
	ArrayList<Block> a2 = new ArrayList<>();
	ArrayList<Block> a3 = new ArrayList<>();

	IIcon icon;

	public BlockOreCrusherController()
	{
		super(TEOreCrusherController.class, TEOreCrusherCasing.class);
		this.setHardness(1.0f);
		this.setResistance(1.0f);
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
		this.setBlockName(Constants.BlockNames.OreCrusherController.toString());

		this.a1.add(CentralRegistry.BlockList.get("OreCrusherCasing"));
		this.a2.add(this);
		this.a3.add(Blocks.air);

		this.map.put('a', this.a1);
		this.map.put('b', this.a2);
		this.map.put('c', this.a3);

		this.MBStruct = new MultiblockStructureDefinition(this.struct, this.map);
	}

	@Override
	public void registerBlockIcons(IIconRegister iir)
	{
		this.icon = iir.registerIcon(Constants.ResourcePrefix + "Machines/OreCrusher/Controller");
	}

	@Override
	public IIcon getIcon(int p_149691_1_, int p_149691_2_)
	{
		return this.icon;
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata)
	{
		return new TEOreCrusherController();
	}

	@Override
	public boolean hasTileEntity(int metadata)
	{
		return true;
	}


	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer p, int p1, float p2, float p3, float p4)
	{
		if(super.onBlockActivated(world, x, y, z, p, p1, p2, p3, p4))
		{
//			p.openGui(ChemicalEnergy.Instance, Constants.GuiIDs.OreCrusher.getID(), world, x, y, z);
			p.addChatComponentMessage(new ChatComponentText("Block OK"));
			return true;
		}

		return false;
	}










	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
	{
		assert (world.getTileEntity(x, y, z) instanceof TEOreCrusherController);
		((TEOreCrusherController) (world.getTileEntity(x, y, z))).DropItems();
		super.breakBlock(world, x, y, z, block, meta);
	}

	@Override
	public TileEntity createNewTileEntity(World var1, int var2)
	{
		return this.createTileEntity(var1, var2);
	}
}
