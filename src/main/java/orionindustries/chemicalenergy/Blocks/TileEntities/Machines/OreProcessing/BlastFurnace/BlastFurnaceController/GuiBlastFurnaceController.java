// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.BlastFurnace.BlastFurnaceController;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Render.Gui.GuiUtilities;

import java.util.ArrayList;
import java.util.List;

public class GuiBlastFurnaceController extends GuiContainer
{
	int xSize = 176;
	int ySize = 199;

	// please set this manually with 'super.tile = this' after calling super(new inventory)
	protected TEBlastFurnaceController tile;

	public GuiBlastFurnaceController(InventoryPlayer inv, TileEntity te)
	{
		super(new InvBlastFurnaceController(inv, te));
		this.tile = (TEBlastFurnaceController) te;
		this.allowUserInput = true;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		// Bind Texture
		this.mc.getTextureManager().bindTexture(Constants.Resources.BlastFurnaceGui.GetResource());
		// set the x for the texture, Total width - textureSize / 2
		par2 = (this.width - this.xSize) / 2;
		// set the y for the texture, Total height - textureSize - 30 (up) / 2,
		int j = (this.height - this.ySize) / 2;
		// draw the texture
		this.drawTexturedModalRect(par2, j, 0, 0, this.xSize, this.ySize);


		// Bind Texture
		this.mc.getTextureManager().bindTexture(TextureMap.locationBlocksTexture);
		double totale = this.tile.GetMaxHeatLevel();
		double actuale = this.tile.GetHeatLevel();

		double barheight = 68.0;
		double height = (actuale / totale) * barheight;

		double proctime = Constants.OtherValues.BlastFurnaceProcessTime.Value();
		double procwidth = 38;
		double sidewidth = 20;






		if(proctime - this.tile.getProgress1() > 0)
		{
			double width = ((proctime - this.tile.getProgress1()) / proctime) * procwidth;
			this.drawGradientRect(par2 + 60, j + 22, (int) Math.min(par2 + 60 + width, par2 + 60 + sidewidth), j + 28, 0xFFFFFFFF, 0xFFFFFFFF);

			if(width > sidewidth)
				this.drawGradientRect((int) (par2 + 60 + sidewidth + 18), j + 22, (int) (par2 + 60 + sidewidth + 16 + (width - sidewidth)),
					j + 28, 0xFFFFFFFF, 0xFFFFFFFF);
		}
		if(proctime - this.tile.getProgress2() > 0)
		{
			double width = ((proctime - this.tile.getProgress2()) / proctime) * procwidth;
			this.drawGradientRect(par2 + 60, j + 40, (int) Math.min(par2 + 60 + width, par2 + 60 + sidewidth), j + 46, 0xFFFFFFFF, 0xFFFFFFFF);

			if(width > sidewidth)
				this.drawGradientRect((int) (par2 + 60 + sidewidth + 16), j + 40, (int) (par2 + 60 + sidewidth + 16 + (width - sidewidth)),
					j + 46, 0xFFFFFFFF, 0xFFFFFFFF);
		}
		if(proctime - this.tile.getProgress3() > 0)
		{
			double width = ((proctime - this.tile.getProgress3()) / proctime) * procwidth;
			this.drawGradientRect(par2 + 60, j + 59, (int) Math.min(par2 + 60 + width, par2 + 60 + sidewidth), j + 65, 0xFFFFFFFF, 0xFFFFFFFF);

			if(width > sidewidth)
				this.drawGradientRect((int) (par2 + 60 + sidewidth + 16), j + 59, (int) (par2 + 60 + sidewidth + 16 + (width - sidewidth)),
					j + 65, 0xFFFFFFFF, 0xFFFFFFFF);
		}
		if(proctime - this.tile.getProgress4() > 0)
		{
			double width = ((proctime - this.tile.getProgress4()) / proctime) * procwidth;
			this.drawGradientRect(par2 + 60, j + 76, (int) Math.min(par2 + 60 + width, par2 + 60 + sidewidth), j + 82, 0xFFFFFFFF, 0xFFFFFFFF);

			if(width > sidewidth)
				this.drawGradientRect((int) (par2 + 60 + sidewidth + 16), j + 76, (int) (par2 + 60 + sidewidth + 16 + (width - sidewidth)),
					j + 82, 0xFFFFFFFF, 0xFFFFFFFF);
		}

		GuiUtilities.RenderBlockTexture(Blocks.lava.getIcon(0, 0), this, par2 + 83, (int)(j + 18 + barheight), 10, (int) height);

		this.drawString(this.fontRendererObj, String.valueOf(this.tile.getRemainingCatalyst()), par2 + 44, j + 98, 0xFFFFFFFF);
	}

	@Override
	public void drawScreen(int mx, int my, float par3)
	{
		super.drawScreen(mx, my, par3);

		int x = (this.width - this.xSize) / 2;
		int y = (this.height - this.ySize) / 2;

		if(mx >= x + 81 && my >= y + 18 && mx <= x + 80 + 15 && my <= y + 18 + 70)
		{
			List<String> list = new ArrayList<>();

			list.add(String.format("%d K", this.tile.GetHeatLevel() + 273));

			this.drawHoveringText(list, mx, my, this.fontRendererObj);
		}
	}
}
