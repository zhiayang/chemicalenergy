// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.OreBlocks;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import orionindustries.chemicalenergy.Utilities.Constants;

public class ItemOreBlock extends ItemBlock
{
	public ItemOreBlock(Block b)
	{
		super(b);
		this.setHasSubtypes(true);
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return "tile." + Constants.ModID + "." + Constants.Ores.values()[stack.getItemDamage()] + "Ore";
	}

	@Override
	public int getMetadata(int par1)
	{
		return par1;
	}
}
