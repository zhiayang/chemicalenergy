// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.OreBlocks;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractBlock;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Utilities.Annotations.RequiresItemBlock;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Utilities;

import java.util.*;

@RequiresItemBlock(ItemBlockClass = ItemOreBlock.class)
public class OreBlock extends AbstractBlock
{
	private IIcon[] icons;
	private Map<Integer, Double> DropWeights = new HashMap<>();

	public OreBlock()
	{
		super();
		this.setHardness(2.0f);
		this.setHarvestLevel("pickaxe", 2);

		this.DropWeights.put(1, 0.90);
		this.DropWeights.put(2, 0.10);
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getSubBlocks(Item item, CreativeTabs tab, List list)
	{
		for(int i = 0; i < Constants.Ores.values().length; i++)
		{
			list.add(new ItemStack(this, 1, i));
		}
	}


	@Override
	public int quantityDropped(Random rand)
	{
		return Utilities.GetWeightedRandomInteger(this.DropWeights);
	}


	@Override
	public ArrayList<ItemStack> getDrops(World world, int x, int y, int z, int metadata, int fortune)
	{
		ItemStack stack = new ItemStack(CentralRegistry.ItemList.get("Material"), Utilities.GetWeightedRandomInteger(this.DropWeights), metadata * 2);
		ArrayList<ItemStack> drops = new ArrayList<>();
		drops.add(stack);

		return drops;
	}

	@Override
	public Item getItemDropped(int meta, Random random, int something)
	{
		return CentralRegistry.ItemList.get("Material");
	}

	@Override
	public int damageDropped(int d)
	{
		return d * 2;
	}

	@Override
	public void registerBlockIcons(IIconRegister iir)
	{
		this.icons = new IIcon[Constants.Ores.values().length];
		for(int i = 0; i < this.icons.length; i++)
		{
			this.icons[i] = iir.registerIcon(Constants.ResourcePrefix + "Ores/" + Constants.Ores.values()[i].name());
		}
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile." + Constants.ModID + ".OreBlock";
	}

	@Override
	public IIcon getIcon(int side, int met)
	{
		if(met < Constants.Ores.values().length)
			return this.icons[met];

		else
		{
			ChemicalEnergy.LogObject.log(Level.ERROR, String.format("Invalid metadata %d for BlockOre", met));
			return this.icons[0];
		}
	}
}
