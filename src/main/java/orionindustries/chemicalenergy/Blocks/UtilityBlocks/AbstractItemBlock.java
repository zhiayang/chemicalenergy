// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import orionindustries.chemicalenergy.ChemicalEnergy;

public class AbstractItemBlock extends ItemBlock
{
	public AbstractItemBlock(Block block)
	{
		super(block);
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
	}
}
