// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Utilities;

public class AbstractBlock extends Block
{
	public AbstractBlock()
	{
		super(Material.rock);
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile." + Constants.ModID + "." + Utilities.UnwrapName(super.getUnlocalizedName());
	}

	@Override
	public void registerBlockIcons(IIconRegister iir)
	{
		this.blockIcon = iir.registerIcon(Constants.ResourcePrefix + Utilities.UnwrapName(super.getUnlocalizedName()));
	}
}
