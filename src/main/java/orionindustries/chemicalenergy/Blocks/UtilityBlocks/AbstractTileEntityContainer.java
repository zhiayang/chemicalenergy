// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public abstract class AbstractTileEntityContainer extends TileEntity implements IInventory
{
	protected ItemStack[] Inventory;
	protected int InventorySize;
	protected AbstractTileEntityContainer(int invsize)
	{
		this.Inventory = new ItemStack[invsize];
		this.InventorySize = invsize;
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound tag = new NBTTagCompound();
		this.writeToNBT(tag);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, tag);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
	{
//		this.readFromNBT(pkt.func_148857_g());
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		NBTTagList nbttaglist = new NBTTagList();

		// save items
		for(int i = 0; i < this.getSizeInventory(); i++)
		{
			if(this.Inventory[i] != null)
			{
				NBTTagCompound nbttagcompound = new NBTTagCompound();
				nbttagcompound.setByte("Slot", (byte)i);
				this.Inventory[i].writeToNBT(nbttagcompound);
				nbttaglist.appendTag(nbttagcompound);
			}
		}
		tag.setTag("Items", nbttaglist);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		NBTTagList nbttaglist = tag.getTagList("Items", net.minecraftforge.common.util.Constants.NBT.TAG_COMPOUND); // ID for compounds

		for(int i = 0; i < nbttaglist.tagCount(); i++)
		{
			NBTTagCompound nbttagcompound = nbttaglist.getCompoundTagAt(i);
			int j = nbttagcompound.getByte("Slot");

			if(j >= 0 && j < this.Inventory.length)
			{
				this.Inventory[j] = ItemStack.loadItemStackFromNBT(nbttagcompound);
			}
		}
	}


	public void DropItems()
	{
		for(int i = 0; i < this.getSizeInventory(); i++)
		{
			ItemStack stack =this.Inventory[i];
			if(stack == null)
				continue;

			EntityItem entityitem = new EntityItem(this.worldObj, (double)this.xCoord, (double)this.yCoord, (double)this.zCoord,
					new ItemStack(stack.getItem(), stack.stackSize, stack.getItemDamage()));

			float f3 = 0.05F;

			if(stack.hasTagCompound())
			{
				entityitem.getEntityItem().setTagCompound((NBTTagCompound)stack.getTagCompound().copy());
			}

			this.worldObj.spawnEntityInWorld(entityitem);
		}
	}

	/**
	 *
	 * @param slot the slot ID
	 * @param stack the stack (uses stack.stackSize)
	 * @return Returns true if the increase succeeded, ie. there was space in the slot. False otherwise.
	 */
	protected boolean IncreaseStack(int slot, ItemStack stack)
	{
		if(this.Inventory[slot] != null && this.CanCombineItems(stack, this.Inventory[slot]))
		{
			if(this.Inventory[slot].stackSize <= this.Inventory[slot].getMaxStackSize() + stack.stackSize)
				this.Inventory[slot].stackSize += stack.stackSize;

			else
				return false;
		}
		else
		{
			this.Inventory[slot] = stack.copy();
		}
		return true;
	}

	/**
	 *
	 * @param slot the slot ID
	 * @param stack the stack (uses stack.stackSize)
	 * @param stacksize the stacksize to use, override stack.stackSize
	 * @return Returns true if the increase succeeded, ie. there was space in the slot. False otherwise.
	 */
	protected boolean IncreaseStack(int slot, ItemStack stack, int stacksize)
	{
		if(this.Inventory[slot] != null && this.CanCombineItems(stack, this.Inventory[slot]))
		{
			if(this.Inventory[slot].stackSize <= this.Inventory[slot].getMaxStackSize() + stacksize)
				this.Inventory[slot].stackSize += stacksize;

			else
				return false;
		}
		else
		{
			this.Inventory[slot] = stack.copy();
		}
		return true;
	}


	protected void DecreaseStack(int slot, int dec)
	{
		if(this.Inventory[slot] != null && this.Inventory[slot].stackSize > 1)
			this.Inventory[slot].stackSize -= dec;

		else
			this.Inventory[slot] = null;
	}

	protected boolean CanCombineItems(ItemStack s1, ItemStack s2)
	{
		return (s1 == null || s2 == null) || (s1.getItem() == s2.getItem() && s1.getItemDamage() == s2.getItemDamage());
	}

	@Override
	public int getSizeInventory()
	{
		return this.InventorySize;
	}

	public ItemStack[] GetInventory()
	{
		return this.Inventory;
	}

	@Override
	public ItemStack getStackInSlot(int var1)
	{
		return this.Inventory[var1];
	}

	@Override
	public ItemStack decrStackSize(int SlotIndex, int Decr)
	{
		ItemStack stack = this.getStackInSlot(SlotIndex);
		if(stack != null)
		{
			if(stack.stackSize <= Decr)
			{
				this.setInventorySlotContents(SlotIndex, null);
			}
			else
			{
				stack = stack.splitStack(Decr);
				if(stack.stackSize == 0)
				{
					this.setInventorySlotContents(SlotIndex, null);
				}
			}
		}
		return stack;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1)
	{
		ItemStack stack = getStackInSlot(var1);
		if(stack != null)
		{
			this.setInventorySlotContents(var1, null);
		}
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack)
	{
		this.Inventory[index] = stack;

		if(stack != null && stack.stackSize > this.getInventoryStackLimit())
		{
			stack.stackSize = this.getInventoryStackLimit();
		}
	}



	@Override
	public String getInventoryName()
	{
		return "An Inventory";
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return true;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1)
	{
		return true;
	}

	@Override
	public void openInventory()
	{

	}

	@Override
	public void closeInventory()
	{

	}

	@Override
	public boolean isItemValidForSlot(int var1, ItemStack var2)
	{
		return true;
	}
}
