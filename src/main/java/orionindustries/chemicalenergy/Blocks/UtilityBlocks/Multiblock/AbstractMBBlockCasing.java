// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock;

import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractBlock;

public class AbstractMBBlockCasing extends AbstractBlock
{
	protected Class<? extends AbstractMBTileController> ControllerClass;
	protected Class<? extends AbstractMBTileCasing> CasingClass;

	protected AbstractMBBlockCasing(Class<? extends AbstractMBTileController> control, Class<? extends AbstractMBTileCasing> casingClass)
	{
		this.CasingClass = casingClass;
		this.ControllerClass = control;
	}

	private void AssertTileEntity(Class<? extends TileEntity> tile)
	{
		assert (tile == this.CasingClass);
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer p, int face, float f1, float f2, float f3)
	{
		this.AssertTileEntity(world.getTileEntity(x, y, z).getClass());
		AbstractMBTileCasing tile = (AbstractMBTileCasing) world.getTileEntity(x, y, z);
		assert tile != null;
		Block controller = world.getBlock(tile.ControllerX, tile.ControllerY, tile.ControllerZ);

		if(controller != null)
		{
			if(world.getTileEntity(tile.ControllerX, tile.ControllerY, tile.ControllerZ) != null)
			{
				if(world.getTileEntity(tile.ControllerX, tile.ControllerY, tile.ControllerZ).getClass() == this.ControllerClass)
				{
					if(((AbstractMBTileController) world.getTileEntity(tile.ControllerX, tile.ControllerY, tile.ControllerZ)).Completed)
					{
						controller.onBlockActivated(world, tile.ControllerX, tile.ControllerY, tile.ControllerZ, p, face, f1, f2, f3);
						return true;
					}
				}
			}
		}

		return false;
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
	{
		this.AssertTileEntity(world.getTileEntity(x, y, z).getClass());

		AbstractMBTileCasing tile = (AbstractMBTileCasing) world.getTileEntity(x, y, z);
		assert tile != null;

		Block controller = world.getBlock(tile.ControllerX, tile.ControllerY, tile.ControllerZ);

		if(controller != null)
		{
			if(world.getTileEntity(tile.ControllerX, tile.ControllerY, tile.ControllerZ) != null)
			{
				if(world.getTileEntity(tile.ControllerX, tile.ControllerY, tile.ControllerZ).getClass() == this.ControllerClass)
				{
					AbstractMBTileController c1 = (AbstractMBTileController) world.getTileEntity(tile.ControllerX, tile.ControllerY, tile.ControllerZ);
					c1.Completed = false;
				}
			}
		}

		super.breakBlock(world, x, y, z, block, meta);
	}
}
