// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractContainerBlock;
import orionindustries.chemicalenergy.Utilities.MultiblockStructureDefinition;

import javax.vecmath.Point3d;
import java.util.List;

public abstract class AbstractMBBlockController extends AbstractContainerBlock
{
	protected MultiblockStructureDefinition MBStruct;
	private Class<? extends AbstractMBTileController> ControllerClass;
	private Class<? extends AbstractMBTileCasing> CasingClass;

	protected AbstractMBBlockController(Class<? extends AbstractMBTileController> controller, Class<? extends AbstractMBTileCasing> casing)
	{
		super(Material.iron);
		this.ControllerClass = controller;
		this.CasingClass = casing;
	}



	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer p, int p1, float p2, float p3, float p4)
	{
		boolean found = false;

		if(!world.isRemote)
		{
			assert world.getTileEntity(x, y, z) != null;
			assert world.getTileEntity(x, y, z).getClass() == this.ControllerClass;

			AbstractMBTileController tile = (AbstractMBTileController) world.getTileEntity(x, y, z);

			if(tile.Completed)
				found = true;
		}

		if(!found)
		{
			boolean xp;
			boolean xn;
			boolean zp;
			boolean zn;

			xp = this.MBStruct.CheckStructure(world, x + 1, y - 1, z);
			xn = this.MBStruct.CheckStructure(world, x - 1, y - 1, z);
			zp = this.MBStruct.CheckStructure(world, x, y - 1, z + 1);
			zn = this.MBStruct.CheckStructure(world, x, y - 1, z - 1);

			found = xp || xn || zp || zn;

			if(!found)
			{
				if(world.isRemote)
					p.addChatMessage(new ChatComponentText("Invalid multiblock structure."));

				return false;
			}

			else
			{
				assert world.getTileEntity(x, y, z) != null;
				assert world.getTileEntity(x, y, z).getClass() == this.ControllerClass;

				AbstractMBTileController tile = (AbstractMBTileController) world.getTileEntity(x, y, z);

				tile.OffsetX = (xp ? 1 : (xn ? -1 : 0));
				tile.OffsetZ = (zp ? 1 : (zn ? -1 : 0));
				tile.Completed = true;

				List<Point3d> blocks = this.MBStruct.GetBlocksOfStructure(x + tile.OffsetX, y - 1, z + tile.OffsetZ);

				for(Point3d point : blocks)
				{
					int px = (int) point.x;
					int py = (int) point.y;
					int pz = (int) point.z;
					int mt = world.getBlockMetadata(px, py, pz);

					if(world.getBlock(px, py, pz).hasTileEntity(mt) && world.getTileEntity(px, py, pz).getClass() == this.CasingClass)
					{
						((AbstractMBTileCasing) world.getTileEntity(px, py, pz)).ControllerX = x;
						((AbstractMBTileCasing) world.getTileEntity(px, py, pz)).ControllerY = y;
						((AbstractMBTileCasing) world.getTileEntity(px, py, pz)).ControllerZ = z;

						world.markBlockForUpdate(px, py, pz);
					}
				}
			}
		}
		return true;
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int meta)
	{
		assert (world.getTileEntity(x, y, z) instanceof AbstractMBTileController);
		((AbstractMBTileController) (world.getTileEntity(x, y, z))).DropItems();
		super.breakBlock(world, x, y, z, block, meta);
	}
}
