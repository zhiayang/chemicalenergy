// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;

public class AbstractMBTileCasing extends TileEntity
{
	public int ControllerX;
	public int ControllerY;
	public int ControllerZ;

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);

		// read the controller values.
		this.ControllerX = tag.getInteger("ControllerX");
		this.ControllerY = tag.getInteger("ControllerY");
		this.ControllerZ = tag.getInteger("ControllerZ");
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);

		// write the controller values.
		tag.setInteger("ControllerX", this.ControllerX);
		tag.setInteger("ControllerY", this.ControllerY);
		tag.setInteger("ControllerZ", this.ControllerZ);
	}

	@Override
	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt)
	{
		this.readFromNBT(pkt.func_148857_g());
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound tag = new NBTTagCompound();
		this.writeToNBT(tag);
		return new S35PacketUpdateTileEntity(this.xCoord, this.yCoord, this.zCoord, 0, tag);
	}
}
