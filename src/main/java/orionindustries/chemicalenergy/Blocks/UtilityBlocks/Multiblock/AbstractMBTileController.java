// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks.Multiblock;

import net.minecraft.nbt.NBTTagCompound;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractTileEntityContainer;

public abstract class AbstractMBTileController extends AbstractTileEntityContainer
{
	public int OffsetX = 0;
	public int OffsetZ = 0;
	public boolean Completed = false;


	protected AbstractMBTileController(int invsize)
	{
		super(invsize);
	}

	@Override
	public void readFromNBT(NBTTagCompound tag)
	{
		super.readFromNBT(tag);
		this.OffsetX = tag.getInteger("OffsetX");
		this.OffsetZ = tag.getInteger("OffsetZ");
		this.Completed = tag.getBoolean("Completed");
	}

	@Override
	public void writeToNBT(NBTTagCompound tag)
	{
		super.writeToNBT(tag);
		tag.setInteger("OffsetX", this.OffsetX);
		tag.setInteger("OffsetZ", this.OffsetZ);
		tag.setBoolean("Completed", this.Completed);
	}
}
