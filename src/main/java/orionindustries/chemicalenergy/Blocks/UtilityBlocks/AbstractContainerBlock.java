// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Blocks.UtilityBlocks;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import orionindustries.chemicalenergy.Utilities.Constants;

public abstract class AbstractContainerBlock extends BlockContainer
{
	protected AbstractContainerBlock(Material m)
	{
		super(m);

	}

	protected AbstractContainerBlock()
	{
		this(Material.rock);
	}

	@Override
	public String getUnlocalizedName()
	{
		return "tile." + Constants.ModID + "." + this.UnwrapBlockName(super.getUnlocalizedName());
	}

	public String UnwrapBlockName(String blockname)
	{
		return blockname.substring(blockname.indexOf('.') + 1);
	}

	@Override
	public void registerBlockIcons(IIconRegister iir)
	{
		this.blockIcon = iir.registerIcon(Constants.ResourcePrefix + this.UnwrapBlockName(super.getUnlocalizedName()));
	}
}
