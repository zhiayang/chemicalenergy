// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;
import org.apache.logging.log4j.Logger;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.CreativeTab;
import orionindustries.chemicalenergy.Utilities.Handlers.GlobalEventHandler;
import orionindustries.chemicalenergy.Utilities.Proxies.CommonProxy;

@Mod(modid = Constants.ModID, name = Constants.ModName, version = Constants.ModVersion)
public class ChemicalEnergy
{
	@Mod.Instance()
	public static ChemicalEnergy Instance;

	@SidedProxy(clientSide = Constants.PackageName + "." + Constants.ModID + ".Utilities.Proxies.ClientProxy",
		serverSide = Constants.PackageName + "." + Constants.ModID + ".Utilities.Proxies.CommonProxy")
	public static CommonProxy Proxy;

	private static CreativeTabs CreativeTab = new CreativeTab(CreativeTabs.getNextID(), Constants.ModID);

	public static CreativeTabs getCreativeTab()
	{
		return CreativeTab;
	}

	public static Logger LogObject;

	/*
		Random design notes about current thing:

		For energy cell frames...

		Wet cells would need acid to work, probably use sulphuric acid:
			1. we get zinc sulphite from the zinc ores, could probably extract the sulphite and
				combine with oxygen

			2. most lead-acid batteries use sulphuric acid.

		Also would use lead. (or lead plates?)

		We would probably rename them 'battery frames' or 'battery cell frames'.

		The basic one would hold one (1) wet cell or one (1) dry cell rack, which holds 6 dry cells.
		The advanced one would hold two wet cells or racks,
		The ultimate one (pending rename) would hold four (4).

		I'm thinking we can use glScale to reduce the size of the loaded models as necessary.


		Wet cells are just one type, lead-acid cells.

		As for dry cells, we should have a couple of 'tiers', based on ease of construction.


		I'm thinking Lithium-Ion as top-tier.
		Probably NiMH as medium tier.
		Wet cells are essentially bottom-tier rechargables.

		For single use cells, due to the unobtainability of materials required to make alkaline cells
		(namely KOH and MnO2) in the game, there will probably be one kind, Zinc-Oxide (or zinc silver) cells.


		Also need to come up with sensible values for RF and RF/t.
		RF/t would probably be used mainly for determining the max RF/t output of battery frames.
		I'm not going to let you put batteries (wet or dry) into machines, that doesn't make sense at all,
		being able to put a bunch of AAs into a large machine.

		I'm contemplating adding RF conduits of my own, but for now we'll stick to redstone conduits and
		ender IO stuff.

		EDIT:
		Apparently lithium ion batteries use cobalt.
		So. Either we screw up TinkersConstruct cobalt rarity, or we make it incompatible, or we
		don't generate so much.

		It kinda works for us really, you don't need that many Li-ion batteries, since they're
		rechargable and hold decent amounts of charge. (probably a few racks)

		And obviously one ingot for one battery is insane, it'll probably be 1 : 10 or some other
		more reasonable ratio for ingot - battery.



		This entire deal with the direction the mod is going is making me somewhat regret using TE as the
		power API... but I don't really like UE and the way things are made...

		Since TE isn't updated to 1.7 yet, I don't think we'll add any power transfer of our own... probably
		let one of the other mods do it, TE3 or not.

		Please note that the actual goal of the mod is to function like EE2 except more tech-like
		and with like larger amounts of power usage.
		Basically, not-free resource transmutation.

		I'm thinking of splitting this power stuff into its own mod, but I'm thinking that since this pushes
		more towards realism, I can probably come up with some kind of more-realistic 'explanation' for
		item transmutation.

		Probably splitting atoms? Most likely.
	*/




	/*
		Ore Processing:

		Assume Copper Ore to be chalcopyrite, tin ore to be cassiterite.
		Because of the slightly more involved process (not just pulveriser -> smelter), we give 2.5 - 3 items per ore?
		Each ore essentially has three steps: crush, purify then smelt.
		I'm thinking crushing would give 2 ore dusts, then smelting would have a chance (50%?) to give 2 per ore dust.


		Process for iron:
		well. you can /choose/ to smelt/macerate whatever.
		OR:

		Hematite (Fe2O3) + CaCO3 --> purified Fe2O3
		2 Fe2O3 + 3 C --> 4 Fe + 2 CO2

		therefore, completely ignoring mole ratios, people can get 4x iron (well... 3.5x - 4x?) from doing it this way.
		also, since "It is a common substance found in rocks in all parts of the world", CaCO3 can be gotten from
		using stone in some kind of machine thingy.

		IF we can get it to work, marble would give a much larger amount of CaCO3 per block.




		Crush: 1 ore -> 2 impure
		Purify: 1 impure -> rand(1, 1.75) pure.

		Smelt: 1 impure -> 1 ingot
		Smelt: 1 pure -> rand(1, 1.10) ingot

		therefore, if crush + purify + smelt, 3-4 ingots
		if crush + smelt, 2 ingots.



		Process for copper:
		Chalcopyrite = CuFeS2

		2 CuFeS2 + 2 SiO2 + 4 O2 ––> Cu2S + 2 FeSiO3 + 3 SO2
		Cu2S + O2 ––> 2 Cu + SO2

		The machine will probably use glass for SiO2.
		The FeSiO3 can be heated in a large furnace thing to produce FeO and SiO2, getting you back your glass,
		if you so desire.

		2 FeO + C ––> Fe + CO2.



		Process for tin:
		Cassiterite = SnO2
		the Cassiterite is first purified then crushed, giving 2 dust per ore

		SnO2 + C ––> CO2 + Sn

		Essentially... smelt it? But only use carbon compounds. Any carbon compound.
		Every crushed cassiterite has a 50% chance of producing 2 tin dust.






		Galena nets lead and some silver, I'm thinking 10 : 1 ratio.

		Galena ore is first purified, then crushed into dust.
		This Galena dust is then smelted in a furnace, the same way as Tin (using carbon compounds)
		There is a 10% chance of getting silver dust per galena dust smelted.







		Process for sphalerite:
		Sphalerite = (Zn, Fe)S

		Purify, crush.
		Smelt with carbon, 30% chance to get 2 zinc per ore dust, 10% chance to get one iron from ore dust. (along with zinc)



		Process for Millerite:
		Millerite = NiS.

		2 NiS + 3 O2 ––> NiO + 2 SO2
		2 NiO + H2 ––> H2O + Ni

		Basically, roast with oxygen (similar to copper)
		Then smelt with hydrogen.

		There'd probably be some kind of gas roaster machine.


		Process for Cobaltite:
		Cobaltite = CoAsS.

		Um. this is a bit repetitive.
		2 CoAsS + 8 O2 ––> 2 CoO + 4 SO2 + 2 As2O3
		2 CoO + C ––> CO2 + 2Co

		probably can find a use for the arsenic trioxide.



		FINALY SPODUMENE
		Spodumene = LiAl(SiO3)2.

		Because we don't really need the aluminium at this point, let's throw it away.

		Spodumene + Cl2 ––> LiCl + Al6Si2O13 + Si2O
		Let's just name that shit 'ore sludge' and throw it away.

		LiCl in an electrolytic cell gives Li and Cl2 gas... which can be reused!
	 */



	@Mod.EventHandler
	public static void preInit(FMLPreInitializationEvent event)
	{
		LogObject = event.getModLog();

		try
		{
			CentralRegistry.RegisterThings();
		}
		catch(ClassNotFoundException e)
		{
			System.err.format("Something went wrong while autosearching for classes.");
			e.printStackTrace();
		}

		Proxy.Register();
	}

	@Mod.EventHandler
	public static void init(FMLInitializationEvent event)
	{
		CentralRegistry.RegisterThingsStage2();
		Proxy.RegisterStage2();

		// register your god damned packets

		// register your god damned event handlers
		MinecraftForge.EVENT_BUS.register(new GlobalEventHandler());
	}

	@Mod.EventHandler
	public static void postInit(FMLPostInitializationEvent event)
	{
		CentralRegistry.RegisterOreDictionaryThings();
	}
}
