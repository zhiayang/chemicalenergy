// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Render.Model.WavefrontObj;

import org.lwjgl.opengl.GL11;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings({ "unchecked" })
public class Object3D
{
	private ArrayList VertexSets = new ArrayList(); // Vertex Coordinates
	private ArrayList VertexNormalSets = new ArrayList(); // Vertex Coordinates Normals
	private ArrayList VertexTextureSets = new ArrayList(); // Vertex Coordinates Textures
	private ArrayList Faces = new ArrayList(); // Array of Faces (vertex sets)
	private ArrayList FaceTextures = new ArrayList(); // Array of of Faces textures
	private ArrayList FaceNormals = new ArrayList(); // Array of Faces normals

	private int ObjectList;
	private int PolyCount = 0;

	//// Statisitcs for drawing ////
	public float toppoint = 0;                    // y+
	public float bottompoint = 0;          // y-
	public float leftpoint = 0;                    // x-
	public float rightpoint = 0;          // x+
	public float farpoint = 0;                    // z-
	public float nearpoint = 0;                    // z+

	public Object3D(BufferedReader ref, boolean centreit) throws IOException
	{
		this.LoadObject(ref);
		if(centreit)
		{
			this.CentreModel();
		}
		this.DrawToOpenGLList();
		this.PolyCount = this.Faces.size();
		this.Cleanup();
	}

	private void Cleanup()
	{
		this.VertexSets.clear();
		this.VertexNormalSets.clear();
		this.VertexTextureSets.clear();
		this.Faces.clear();
		this.FaceTextures.clear();
		this.FaceNormals.clear();
	}

	private void LoadObject(BufferedReader br) throws IOException
	{
		int linecounter = 0;
		try
		{
			String newline;
			boolean firstpass = true;

			while(((newline = br.readLine()) != null))
			{
				linecounter++;
				newline = newline.trim();
				if(newline.length() > 0)
				{
					if(newline.charAt(0) == 'v' && newline.charAt(1) == ' ')
					{
						float[] coords = new float[4];
						String[] coordstext;
						coordstext = newline.split("\\s+");
						for(int i = 1; i < coordstext.length; i++)
						{
							coords[i - 1] = Float.valueOf(coordstext[i]);
						}
						//// check for farpoints ////
						if(firstpass)
						{
							this.rightpoint = coords[0];
							this.leftpoint = coords[0];
							this.toppoint = coords[1];
							this.bottompoint = coords[1];
							this.nearpoint = coords[2];
							this.farpoint = coords[2];
							firstpass = false;
						}
						if(coords[0] > this.rightpoint)
						{
							this.rightpoint = coords[0];
						}
						if(coords[0] < this.leftpoint)
						{
							this.leftpoint = coords[0];
						}
						if(coords[1] > this.toppoint)
						{
							this.toppoint = coords[1];
						}
						if(coords[1] < this.bottompoint)
						{
							this.bottompoint = coords[1];
						}
						if(coords[2] > this.nearpoint)
						{
							this.nearpoint = coords[2];
						}
						if(coords[2] < this.farpoint)
						{
							this.farpoint = coords[2];
						}
						/////////////////////////////
						this.VertexSets.add(coords);
					}
					if(newline.charAt(0) == 'v' && newline.charAt(1) == 't')
					{
						float[] coords = new float[4];
						String[] coordstext;
						coordstext = newline.split("\\s+");
						for(int i = 1; i < coordstext.length; i++)
						{
							coords[i - 1] = Float.valueOf(coordstext[i]);
						}
						this.VertexTextureSets.add(coords);
					}
					if(newline.charAt(0) == 'v' && newline.charAt(1) == 'n')
					{
						float[] coords = new float[4];
						String[] coordstext;
						coordstext = newline.split("\\s+");
						for(int i = 1; i < coordstext.length; i++)
						{
							coords[i - 1] = Float.valueOf(coordstext[i]);
						}
						this.VertexNormalSets.add(coords);
					}
					if(newline.charAt(0) == 'f' && newline.charAt(1) == ' ')
					{
						String[] coordstext = newline.split("\\s+");
						int[] v = new int[coordstext.length - 1];
						int[] vt = new int[coordstext.length - 1];
						int[] vn = new int[coordstext.length - 1];

						for(int i = 1; i < coordstext.length; i++)
						{
							String fixstring = coordstext[i].replaceAll("//", "/0/");
							String[] tempstring = fixstring.split("/");
							v[i - 1] = Integer.valueOf(tempstring[0]);
							if(tempstring.length > 1)
							{
								vt[i - 1] = Integer.valueOf(tempstring[1]);
							}
							else
							{
								vt[i - 1] = 0;
							}
							if(tempstring.length > 2)
							{
								vn[i - 1] = Integer.valueOf(tempstring[2]);
							}
							else
							{
								vn[i - 1] = 0;
							}
						}
						this.Faces.add(v);
						this.FaceTextures.add(vt);
						this.FaceNormals.add(vn);
					}
				}
			}

		}
		catch(IOException e)
		{
			System.out.println("Failed to read file: " + br.toString());
		}
		catch(NumberFormatException e)
		{
			System.out.println("Malformed OBJ (on line " + linecounter + "): " + br.toString() + "\r \r" + e.getMessage());
		}

	}

	private void CentreModel()
	{
		float xshift = (this.rightpoint - this.leftpoint) / 2f;
		float yshift = (this.toppoint - this.bottompoint) / 2f;
		float zshift = (this.nearpoint - this.farpoint) / 2f;

		for(int i = 0; i < this.VertexSets.size(); i++)
		{
			float[] coords = new float[4];

			coords[0] = ((float[]) (this.VertexSets.get(i)))[0] - this.leftpoint - xshift;
			coords[1] = ((float[]) (this.VertexSets.get(i)))[1] - this.bottompoint - yshift;
			coords[2] = ((float[]) (this.VertexSets.get(i)))[2] - this.farpoint - zshift;

			this.VertexSets.set(i, coords); // = coords;
		}

	}

	public float getXWidth()
	{
		float returnval;
		returnval = this.rightpoint - this.leftpoint;
		return returnval;
	}

	public float getYHeight()
	{
		float returnval;
		returnval = this.toppoint - this.bottompoint;
		return returnval;
	}

	public float getZDepth()
	{
		float returnval;
		returnval = this.nearpoint - this.farpoint;
		return returnval;
	}

	public int getPolyCount()
	{
		return this.PolyCount;
	}

	public void DrawToOpenGLList()
	{

		this.ObjectList = GL11.glGenLists(1);

		GL11.glNewList(this.ObjectList, GL11.GL_COMPILE);
		for(int i = 0; i < this.Faces.size(); i++)
		{
			int[] tempfaces = (int[]) (this.Faces.get(i));
			int[] tempfacesnorms = (int[]) (this.FaceNormals.get(i));
			int[] tempfacestexs = (int[]) (this.FaceTextures.get(i));

			//// Quad Begin Header ////
			int polytype;
			if(tempfaces.length == 3)
			{
				polytype = GL11.GL_TRIANGLES;
			}
			else if(tempfaces.length == 4)
			{
				polytype = GL11.GL_QUADS;
			}
			else
			{
				polytype = GL11.GL_POLYGON;
			}
			GL11.glBegin(polytype);
			////////////////////////////

			for(int w = 0; w < tempfaces.length; w++)
			{
				if(tempfacesnorms[w] != 0)
				{
					float normtempx = ((float[]) this.VertexNormalSets.get(tempfacesnorms[w] - 1))[0];
					float normtempy = ((float[]) this.VertexNormalSets.get(tempfacesnorms[w] - 1))[1];
					float normtempz = ((float[]) this.VertexNormalSets.get(tempfacesnorms[w] - 1))[2];
					GL11.glNormal3f(normtempx, normtempy, normtempz);
				}

				if(tempfacestexs[w] != 0)
				{
					float textempx = ((float[]) this.VertexTextureSets.get(tempfacestexs[w] - 1))[0];
					float textempy = ((float[]) this.VertexTextureSets.get(tempfacestexs[w] - 1))[1];
					float textempz = ((float[]) this.VertexTextureSets.get(tempfacestexs[w] - 1))[2];
					GL11.glTexCoord3f(textempx, 1f - textempy, textempz);
				}

				float tempx = ((float[]) this.VertexSets.get(tempfaces[w] - 1))[0];
				float tempy = ((float[]) this.VertexSets.get(tempfaces[w] - 1))[1];
				float tempz = ((float[]) this.VertexSets.get(tempfaces[w] - 1))[2];
				GL11.glVertex3f(tempx, tempy, tempz);
			}


			//// Quad End Footer /////
			GL11.glEnd();
			///////////////////////////


		}
		GL11.glEndList();
	}

	public void Render()
	{
		GL11.glCallList(this.ObjectList);
	}

}
