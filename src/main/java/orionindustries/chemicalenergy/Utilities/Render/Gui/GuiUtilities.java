// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Render.Gui;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.util.IIcon;
import orionindustries.chemicalenergy.Utilities.Constants;

public class GuiUtilities
{
	public static void RenderBlockTexture(IIcon texture, GuiContainer container, int x, int y, int width, int height)
	{
		int remaining = height;
		while(remaining > 0)
		{
			container.drawTexturedModelRectFromIcon(x, y - remaining, texture, width, Math.min(remaining, width));
			remaining -= width;
		}
	}

	public static void RenderEnergyBar(GuiContainer container, int x, int y, int height)
	{
		container.mc.getTextureManager().bindTexture(Constants.Resources.EnergyBar.GetResource());
		container.drawTexturedModalRect(x, y, 0, 0, 12, height);
	}
}
