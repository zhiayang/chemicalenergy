// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities;

import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MachineRecipe
{
	private boolean ConsumesCatalyst;
	private List<ItemStack> Inputs;
	private List<ItemStack> Outputs;
	private Constants.CatalystTypes CatalystType;
	private int CatalystConsumption;
	private Map<Integer, Double> Weights;

	public static MachineRecipe GetRecipe(List<MachineRecipe> recipes, ItemStack Input)
	{
		return GetRecipe(recipes, new ArrayList<>(Arrays.asList(Input)));
	}

	public static MachineRecipe GetRecipe(List<MachineRecipe> recipes, List<ItemStack> Inputs)
	{
		for(MachineRecipe recipe : recipes)
		{
			for(int i = 0; i < Inputs.size(); i++)
				if(Inputs.get(i).isItemEqual(recipe.Inputs.get(i)))
					return recipe;
		}

		return null;
	}

	public static List<ItemStack> GetRecipeOutputs(List<MachineRecipe> recipes, List<ItemStack> Inputs)
	{
		return GetRecipe(recipes, Inputs).Outputs;
	}

	public static List<ItemStack> GetRecipeOutputs(List<MachineRecipe> recipes, ItemStack Input)
	{
		return GetRecipeOutputs(recipes, new ArrayList<>(Arrays.asList(Input)));
	}



	public static List<ItemStack> GetRecipeInputs(List<MachineRecipe> recipes, List<ItemStack> Inputs)
	{
		for(MachineRecipe recipe : recipes)
		{
			for(int i = 0; i < Inputs.size(); i++)
				if(Inputs.get(i).isItemEqual(recipe.Inputs.get(i)))
					return recipe.Inputs;
		}

		return null;
	}

	public static List<ItemStack> GetRecipeInputs(List<MachineRecipe> recipes, ItemStack Input)
	{
		return GetRecipeInputs(recipes, new ArrayList<>(Arrays.asList(Input)));
	}







	/**
	 * @param Inputs  List of inputs, will only process when all are present.
	 * @param Outputs List of outputs.
	 */
	public MachineRecipe(List<ItemStack> Inputs, List<ItemStack> Outputs)
	{
		this(Inputs, Outputs, null, false, 0, null);
	}

	/**
	 * @param Input  Input itemstack.
	 * @param Output Output itemstack.
	 */
	public MachineRecipe(ItemStack Input, ItemStack Output)
	{
		this(Input, Output, null, false, 0, null);
	}

	/**
	 * @param Input  Input itemstack.
	 * @param Output Output itemstack.
	 */
	public MachineRecipe(ItemStack Input, ItemStack Output, Map<Integer, Double> weights)
	{
		this(Input, Output, null, false, 0, weights);
	}


	/**
	 * @param Input       Input itemstack.
	 * @param Output      Output itemstack.
	 * @param consumesCatalyst For the blast furnace, set to true if the recipe needs a source of carbon.
	 */
	public MachineRecipe(ItemStack Input, ItemStack Output, Constants.CatalystTypes catalyst, boolean consumesCatalyst, int catalystConsumption)
	{
		this(new ArrayList<>(Arrays.asList(Input)), new ArrayList<>(Arrays.asList(Output)), catalyst, consumesCatalyst, catalystConsumption, null);
	}

	/**
	 * @param Input       Input itemstack.
	 * @param Output      Output itemstack.
	 * @param consumesCatalyst For the blast furnace, set to true if the recipe needs a source of carbon.
	 */
	public MachineRecipe(ItemStack Input, ItemStack Output, Constants.CatalystTypes catalyst, boolean consumesCatalyst, int catcon, Map<Integer, Double> weights)
	{
		this(new ArrayList<>(Arrays.asList(Input)), new ArrayList<>(Arrays.asList(Output)), catalyst, consumesCatalyst, catcon, weights);
	}










	/**
	 * @param inputs      List of inputs, will only process when all are present.
	 * @param outputs     List of outputs.
	 * @param consumesCatalyst For the blast furnace, set to true if the recipe needs a source of carbon.
	 */
	public MachineRecipe(List<ItemStack> inputs, List<ItemStack> outputs, Constants.CatalystTypes catalyst, boolean consumesCatalyst, int catcon,
	                     Map<Integer, Double> weights)
	{
		this.Inputs = inputs;
		this.Outputs = outputs;
		this.ConsumesCatalyst = consumesCatalyst;
		this.CatalystType = catalyst;
		this.CatalystConsumption = catcon;
		this.Weights = weights;
	}

	public List<ItemStack> GetInputs()
	{
		return this.Inputs;
	}

	public List<ItemStack> GetOutputs()
	{
		return this.Outputs;
	}

	public boolean DoesConsumeCatalyst()
	{
		return this.ConsumesCatalyst;
	}

	public Constants.CatalystTypes GetCatalyst()
	{
		return this.CatalystType;
	}

	public int GetWeightedMultiplier()
	{
		return Utilities.GetWeightedRandomInteger(this.Weights);
	}

	public boolean CheckIsValidCatalyst(ItemStack stack)
	{
		if(stack == null)
			return false;

		switch(this.CatalystType)
		{
			case Carbon:
				return Constants.CarbonItems.containsKey(stack.getItem());
		}

		return false;
	}

	public int GetCatalystConsumption()
	{
		return this.CatalystConsumption;
	}

	public int GetCatalystValue(ItemStack stack)
	{
		if(stack == null)
			return 0;

		switch(this.CatalystType)
		{
			case Carbon:
				if(Constants.CarbonItems.containsKey(stack.getItem()))
					return Constants.CarbonItems.get(stack.getItem());
		}

		return 0;
	}

	public Constants.CatalystTypes GetCatalystType()
	{
		return this.CatalystType;
	}
}
