// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Annotations;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.tileentity.TileEntity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RequiresTileEntity
{
	int GuiID();

	Class<? extends TileEntity> TileEntityClass();
	Class<? extends Container> ContainerClass() default DefaultContainer.class;
	Class<? extends GuiContainer> GuiContainerClass() default DefaultGuiContainer.class;







	public static abstract class DefaultContainer extends Container
	{
	}

	public static abstract class DefaultGuiContainer extends GuiContainer
	{
		public DefaultGuiContainer(Container par1Container)
		{
			super(par1Container);
		}
	}
}
