// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

public class Utilities
{
	/**
	 * @param name name to unwrap
	 * @return a String without the leading item type, eg. 'item.' or 'tile.'
	 */
	public static String UnwrapName(String name)
	{
		return name.substring(name.indexOf('.') + 1);
	}

	/**
	 * @param number                the number to format
	 * @param threshold             the minimum value before a unit is increased
	 * @param useinteger            whether or not to use an integer when possible (ie. don't display decimal points)
	 * @param useIntegerForBaseUnit same as useInteger, except it only applies when the unit scale is 1
	 * @return formatted string.
	 */
	public static String FormatNumber(double number, int threshold, boolean useinteger, boolean useIntegerForBaseUnit)
	{
		double num = number;
		int ume = 0;
		double disp;

		while(number > threshold)
		{
			ume++;
			number /= 1000;
		}

		disp = num / Math.pow(1000.0, ume);

		if(disp == (long) disp && useinteger || (ume == 0 && useIntegerForBaseUnit))
			return String.format("%d %s", (long) disp, Constants.UnitPrefixes.get(ume));

		else
			return String.format("%.2f %s", disp, Constants.UnitPrefixes.get(ume));
	}

	/**
	 * Simple wrapper, default params are number, ~10000, true, true
	 *
	 * @param number the number to format
	 * @return formatted string
	 */
	public static String FormatNumber(double number)
	{
		return FormatNumber(number, Constants.DefaultThreshold, true, true);
	}

	/**
	 * Simple wrapper, default params are number, ~10000, useInteger, true
	 *
	 * @param number     the number to format
	 * @param useinteger whether or not to use an integer when possible (ie. don't display decimal points)
	 * @return formatted string
	 */
	public static String FormatNumber(double number, boolean useinteger)
	{
		return FormatNumber(number, Constants.DefaultThreshold, useinteger, true);
	}

	/**
	 * Simple wrapper, default params are number, ~10000, useInteger, useIntegerForBaseUnit
	 *
	 * @param number                the number to format
	 * @param useinteger            whether or not to use an integer when possible (ie. don't display decimal points)
	 * @param useIntegerForBaseUnit same as useInteger, except it only applies when the unit scale is 1
	 * @return formatted string
	 */
	public static String FormatNumber(double number, boolean useinteger, boolean useIntegerForBaseUnit)
	{
		return FormatNumber(number, Constants.DefaultThreshold, useinteger, useIntegerForBaseUnit);
	}









	/**
	 * Get a weighted random value. Weights is of the form (Integer, Double), where Integer is one of the possible
	 * values and Double is the weight, from 0.0 - 1.0. The weights do not need to add up to 0.
	 *
	 * @param weights the map of weigted values
	 * @return one of the values
	 */
	public static int GetWeightedRandomInteger(Map<Integer, Double> weights)
	{
		// Compute the total weight of all items together
		double totalWeight = 0.0;
		if(weights == null)
			return 1;

		for(double i : weights.values())
		{
			totalWeight += i;
		}

		// Now choose a random item
		int randomIndex = -1;
		double random = Math.random() * totalWeight;
		for(int i = 0; i < weights.size(); ++i)
		{
			random -= (double) weights.values().toArray()[i];
			if(random <= 0.0d)
			{
				randomIndex = i;
				break;
			}
		}

		return (int) weights.keySet().toArray()[randomIndex];
	}









	/**
	 * Recursively gets a list of classes in the package.
	 *
	 * @param pckgname name of the package to search in
	 * @return a Set of classes
	 * @throws ClassNotFoundException
	 */
	public static Set<Class> GetClassesInPackage(String pckgname) throws ClassNotFoundException
	{
		// This will hold a list of directories matching the pckgname. There may be more than one if a package is split over multiple jars/paths
		ArrayList<File> directories = new ArrayList<>();
		try
		{
			ClassLoader cld = Thread.currentThread().getContextClassLoader();
			if(cld == null)
			{
				throw new ClassNotFoundException("Can't get class loader.");
			}
			String path = pckgname.replace('.', '/');
			// Ask for all resources for the path
			Enumeration<URL> resources = cld.getResources(path);
			while(resources.hasMoreElements())
			{
				directories.add(new File(URLDecoder.decode(resources.nextElement().getPath(), "UTF-8")));
			}
		}
		catch(NullPointerException x)
		{
			throw new ClassNotFoundException(pckgname + " does not appear to be a valid package (Null pointer exception)");
		}
		catch(UnsupportedEncodingException encex)
		{
			throw new ClassNotFoundException(pckgname + " does not appear to be a valid package (Unsupported encoding)");
		}
		catch(IOException ioex)
		{
			throw new ClassNotFoundException("IOException was thrown when trying to get all resources for " + pckgname);
		}

		Set<Class> classes = new HashSet<>();
		// For every directory identified capture all the .class files
		for(File directory : directories)
		{
			if(directory.exists())
			{
				// Get the list of the files contained in the package
				File[] files = directory.listFiles();
				if(files == null)
					continue;

				for(File file : files)
				{
					// we are only interested in .class files
					if(file.getName().endsWith(".class"))
					{
						// removes the .class extension
						try
						{
							classes.add(Class.forName(pckgname + '.' + file.getName().substring(0, file.getName().length() - 6)));
						}
						catch(NoClassDefFoundError e)
						{
							// do nothing. this class hasn't been found by the loader, and we don't care.
						}
					}
					else if(file.isDirectory())
						classes.addAll(GetClassesInPackage(pckgname + "." + file.getName()));
				}
			}
			else
			{
				throw new ClassNotFoundException(pckgname + " (" + directory.getPath() + ") does not appear to be a valid package");
			}
		}
		return classes;
	}
}
