// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Proxies;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraftforge.client.MinecraftForgeClient;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.Renderers.BasicEnergyCellFrameISBRH;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.Renderers.BasicEnergyCellFrameTESR;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.TEBasicEnergyCellFrame;
import orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack.GuiBatteryRack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.LeadAcidBattery;
import orionindustries.chemicalenergy.Items.Energy.Batteries.Renderers.LeadAcidBatteryRenderer;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;
import orionindustries.chemicalenergy.Utilities.Constants;

public class ClientProxy extends CommonProxy
{
	@Override
	public void Register()
	{
		super.Register();

		// register TESRs
		ClientRegistry.bindTileEntitySpecialRenderer(TEBasicEnergyCellFrame.class, new BasicEnergyCellFrameTESR());
		RenderingRegistry.registerBlockHandler(new BasicEnergyCellFrameISBRH());

		// register item renderers
		MinecraftForgeClient.registerItemRenderer(LeadAcidBattery.Instance, new LeadAcidBatteryRenderer());


		CentralRegistry.ItemGuiMapping.put(Constants.GuiIDs.BatteryRack.getID(), GuiBatteryRack.class);
	}

	@Override
	public void RegisterStage2()
	{
		super.RegisterStage2();
	}

	@Override
	public void RegisterClientTileEntity(int GuiID, Class<? extends GuiContainer> GuiClass)
	{
		CentralRegistry.TEGuiMapping.put(GuiID, GuiClass);
	}
}
