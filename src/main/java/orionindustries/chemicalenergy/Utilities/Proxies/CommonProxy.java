// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Proxies;

import net.minecraft.client.gui.inventory.GuiContainer;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;

public class CommonProxy
{
	public void Register()
	{
	}

	public void RegisterStage2()
	{
	}

	public void RegisterClientTileEntity(int GuiID, Class<? extends GuiContainer> GuiClass)
	{
		CentralRegistry.TEGuiMapping.put(GuiID, GuiClass);
	}
}
