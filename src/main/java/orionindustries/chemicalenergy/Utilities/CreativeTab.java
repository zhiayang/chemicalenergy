// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTab extends CreativeTabs
{
	public CreativeTab(int par1, String par2Str)
	{
		super(par1, par2Str);
	}

	@Override
	public Item getTabIconItem()
	{
		return CentralRegistry.CreativeTabItem;
	}
}
