// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class Constants
{
	// global things
	public static final String PackageName = "orionindustries";

	public static final String ModID = "chemicalenergy";
	public static final String ResourcePrefix = ModID + ":";
	public static final String ModName = "Chemical Energy";
	public static final String ModVersion = "1.0";
	public static final int RFToMJRatio = 10;
	public static final int DefaultThreshold = 100000;
	public static final int BatteryLevels = 21;

	public static final Map<Integer, String> UnitPrefixes;
	public static final Map<Item, Integer> CarbonItems = new HashMap<>();


	static
	{
		UnitPrefixes = new HashMap<>();
		UnitPrefixes.put(0, "");
		UnitPrefixes.put(1, "k");
		UnitPrefixes.put(2, "m");
		UnitPrefixes.put(3, "g");
		UnitPrefixes.put(4, "t");
	}



	// actual mod data -- energy cell values.
	public static enum EnergyCellValues
	{
		BasicEnergyCellFrame(80000, 100, 80),
		AdvancedEnergyCellFrame(2560000, 200, 400);

		private final int MaxEnergy;
		private final int MaxBCInput;
		private final int BatteryTransferRate;

		EnergyCellValues(final int x, final int mbi, final int battrate)
		{
			this.MaxEnergy = x;
			this.MaxBCInput = mbi;
			this.BatteryTransferRate = battrate;
		}

		public int MaxEnergy()
		{
			return this.MaxEnergy;
		}

		public int MaxBCInput()
		{
			return this.MaxBCInput;
		}

		public int BatteryTransferRate()
		{
			return this.BatteryTransferRate;
		}
	}

	public static enum BatteryValues
	{
		LeadAcidRechargable(16000, 80),
		NickelMetalHydrideRechargable(320000, 400),
		LithiumIonRechargable(6400000, 1600);

		private final int MaxEnergy;
		private final int TransferRate;

		BatteryValues(final int x, final int transferrate)
		{
			this.MaxEnergy = x;
			this.TransferRate = transferrate;
		}

		public int MaxEnergy()
		{
			return this.MaxEnergy;
		}

		public int TransferRate()
		{
			return this.TransferRate;
		}
	}









	public static enum OtherValues
	{
		BlastFurnaceMaxHeatLevel(2400),
		BlastFurnaceProcessTime(20);

		private final int Value;

		OtherValues(final int x)
		{
			this.Value = x;
		}

		public int Value()
		{
			return this.Value;
		}
	}

	public static enum GuiIDs
	{
		BasicEnergyCellFrame(0),
		AdvancedEnergyCellFrame(1),
		QuantumEnergyCellFrame(2),
		BlastFurnace(3),
		BatteryRack(4),
		OreCrusher(5),;



		private final int ID;

		private GuiIDs(int GuiID)
		{
			this.ID = GuiID;
		}

		public int getID()
		{
			return this.ID;
		}
	}






	// block names
	public static enum BlockNames
	{
		TestBlock("TestBlock"),
		OreBlock("OreBlock"),

		BlastFurnaceCasing("BlastFurnaceCasing"),
		BlastFurnaceController("BlastFurnaceController"),

		OreCrusherCasing("OreCrusherCasing"),
		OreCrusherController("OreCrusherController"),

		BasicEnergyCellFrame("BasicEnergyCellFrame"),
		AdvancedEnergyCellFrame("AdvancedEnergyCellFrame"),
		QuantumEnergyCellFrame("QuantumEnergyCellFrame"),;

		private BlockNames(final String text)
		{

		}

		@NotNull
		@Override
		public String toString()
		{
			return this.name();
		}
	}



	public static enum Resources
	{
		BasicEnergyCellFrameGui(new ResourceLocation(ResourcePrefix + "textures/gui/EnergyCellFrames/Basic.png")),
		AdvancedEnergyCellFrameGui(new ResourceLocation(ResourcePrefix + "textures/gui/EnergyCellFrames/Advanced.png")),
		QuantumEnergyCellFrameGui(new ResourceLocation(ResourcePrefix + "textures/gui/EnergyCellFrames/Quantum.png")),

		BasicEnergyCellFrameModel(new ResourceLocation(ResourcePrefix + "models/BasicEnergyCellFrame.obj")),
		AdvancedEnergyCellFrameModel(new ResourceLocation(ResourcePrefix + "models/AdvancedEnergyCellFrame.obj")),
		QuantumEnergyCellFrameModel(new ResourceLocation(ResourcePrefix + "models/QuantumEnergyCellFrame.obj")),

		BasicEnergyCellFrameTexture(new ResourceLocation(ResourcePrefix + "models/BasicEnergyCellFrame.png")),
		AdvancedEnergyCellFrameTexture(new ResourceLocation(ResourcePrefix + "models/AdvancedEnergyCellFrame.png")),
		QuantumEnergyCellFrameTexture(new ResourceLocation(ResourcePrefix + "models/QuantumEnergyCellFrame.png")),


		WetCellModel(new ResourceLocation(ResourcePrefix + "models/WetCell.obj")),
		WetCellTexture(new ResourceLocation(ResourcePrefix + "models/WetCell.png")),

		DryCellRackModel(new ResourceLocation(ResourcePrefix + "models/DryCellRack.obj")),
		DryCellRackTexture(new ResourceLocation(ResourcePrefix + "models/DryCellRack.png")),

		DryCellModel(new ResourceLocation(ResourcePrefix + "models/DryCell.obj")),
		DryCellTexture(new ResourceLocation(ResourcePrefix + "models/DryCell.png")),

		DryCellGreenGauge(new ResourceLocation(ResourcePrefix + "textures/items/Batteries/BatteryLevelGreen.png")),
		DryCellPurpleGauge(new ResourceLocation(ResourcePrefix + "textures/items/Batteries/BatteryLevelPurple.png")),
		DryCellBlueGauge(new ResourceLocation(ResourcePrefix + "textures/items/Batteries/BatteryLevelBlue.png")),
		DryCellBlackGauge(new ResourceLocation(ResourcePrefix + "textures/items/Batteries/BatteryLevelBlack.png")),
		DryCellGaugeOverlay(new ResourceLocation(ResourcePrefix + "textures/items/Batteries/Overlay.png")),



		BlastFurnaceGui(new ResourceLocation(ResourcePrefix + "textures/gui/Machines/BlastFurnace.png")),
		BatteryRackGui(new ResourceLocation(ResourcePrefix + "textures/gui/Misc/BatteryRack.png")),

		EnergyBar(new ResourceLocation(ResourcePrefix + "textures/gui/Misc/EnergyBar.png")),;





		private final ResourceLocation relloc;

		private Resources(final ResourceLocation r)
		{
			this.relloc = r;
		}

		public ResourceLocation GetResource()
		{
			return this.relloc;
		}
	}


	public static enum CatalystTypes
	{
		Carbon,
		SiliconDioxide
	}


	public static enum Ores
	{
		Copper,
		Tin,
		Chlorargyrite,
		Galena,
		Sphalerite,
		Cobaltite,
		Spodumene,
		Millerite,;
	}

	public static enum Materials
	{
		// Ore process dusts
		ImpureChalcopyriteDust("CuFeS\u2082", "ImpureChalcopyrite"),
		ChalcopyriteDust("CuFeS\u2082", "Chalcopyrite"),

		ImpureCassiteriteDust("SnO\u2082", "ImpureCassiterite"),
		CassiteriteDust("SnO\u2082", "Cassiterite"),

		ImpureChlorargyriteDust("AgCl", "ImpureChlorargyrite"),
		ChlorargyriteDust("AgCl", "Chlorargyrite"),

		ImpureGalenaDust("PbS, Ag\u2082S", "ImpureGalena"),
		GalenaDust("PbS, Ag\u2082S", "Galena"),

		ImpureSphaleriteDust("ZnS, FeS", "ImpureSphalerite"),
		SphaleriteDust("ZnS, FeS", "Sphalerite"),

		ImpureCobaltiteDust("CoAsS", "ImpureCobaltite"),
		CobaltiteDust("CoAsS", "Cobaltite"),

		ImpureSpodumeneDust("LiAl(SiO\u2083)\u2082", "ImpureSpodumene"),
		SpodumeneDust("LiAl(SiO\u2083)\u2082", "Spodumene"),

		ImpureMilleriteDust("NiS", "ImpureMillerite"),
		MilleriteDust("NiS", "Millerite"),

		// not generated in world, we use iron ore.
		ImpureHaematiteDust("Fe\u2082O\u2083", "ImpureHaematite"),
		HaematiteDust("Fe\u2082O\u2083", "Haematite"),

		// by product transitional materials
		CopperSulphideDust("Cu\u2082S", "CopperSulphide"),
		NickelOxide("NiO", "NickelOxide"),
		CobaltOxide("CoO", "CobaltOxide"),


		// purified dusts
		CopperDust("Cu", "Copper"),
		TinDust("Sn", "Tin"),
		IronDust("Fe", "Iron"),
		LeadDust("Pb", "Lead"),
		SilverDust("Ag", "Silver"),
		ZincDust("Zn", "Zinc"),
		NickelDust("Ni", "Nickel"),
		CobaltDust("Co", "Cobalt"),
		LithiumDust("Li", "Lithium"),;


		private final String desc;
		private final String dictname;

		private Materials(String de, String oredictname)
		{
			this.desc = de;
			this.dictname = oredictname;
		}

		public String GetDescription()
		{
			return this.desc;
		}

		public String GetOreDictName()
		{
			return this.dictname;
		}

		public static int GetMetadataFromName(String name)
		{
			for(int i = 0; i < values().length; i++)
				if(values()[i].name().equals(name))
					return i;

			return 0;
		}
	}


	public static enum MetalIngots
	{
		Copper,
		Tin,
		Lead,
		Silver,
		Zinc,
		Nickel,
		Cobalt,
		Lithium,;

		public static int GetMetadataFromName(String name)
		{
			for(int i = 0; i < values().length; i++)
				if(values()[i].name().equals(name))
					return i;

			return 0;
		}
	}


}
