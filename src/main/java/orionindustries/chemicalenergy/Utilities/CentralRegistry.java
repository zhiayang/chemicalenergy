// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.oredict.OreDictionary;
import orionindustries.chemicalenergy.Blocks.OreBlocks.OreBlock;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.AdvancedEnergyCellFrame.BlockAdvancedEnergyCellFrame;
import orionindustries.chemicalenergy.Blocks.TileEntities.Energy.EnergyCellFrames.BasicEnergyCellFrame.BlockBasicEnergyCellFrame;
import orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.BlastFurnace.BlastFurnaceCasing.BlockBlastFurnaceCasing;
import orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.BlastFurnace.BlastFurnaceController.BlockBlastFurnaceController;
import orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherCasing.BlockOreCrusherCasing;
import orionindustries.chemicalenergy.Blocks.TileEntities.Machines.OreProcessing.OreCrusher.OreCrusherController.BlockOreCrusherController;
import orionindustries.chemicalenergy.Blocks.UtilityBlocks.AbstractBlock;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack.InventoryBatteryRack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack.ItemBatteryRack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.LeadAcidBattery;
import orionindustries.chemicalenergy.Items.Energy.Batteries.LithiumIonBattery;
import orionindustries.chemicalenergy.Items.Energy.Batteries.NiMHBattery;
import orionindustries.chemicalenergy.Items.Materials.ItemMaterial;
import orionindustries.chemicalenergy.Items.Materials.ItemMetalIngot;
import orionindustries.chemicalenergy.Items.UtilityItems.DebugItem;
import orionindustries.chemicalenergy.Utilities.Annotations.RequiresItemBlock;
import orionindustries.chemicalenergy.Utilities.Annotations.RequiresTileEntity;
import orionindustries.chemicalenergy.Utilities.Handlers.GlobalGUIHandler;

import java.util.*;

public class CentralRegistry
{
	public static Map<String, Block> BlockList = new HashMap<>();
	public static Map<String, Item> ItemList = new HashMap<>();
	public static Item CreativeTabItem;

	public static List<MachineRecipe> BlastFurnaceRecipes = new ArrayList<>();
	public static Map<Integer, Class<? extends GuiContainer>> TEGuiMapping = new HashMap<>();
	public static Map<Integer, Class<? extends GuiContainer>> ItemGuiMapping = new HashMap<>();

	public static Map<Integer, Class<? extends Container>> TEInvMapping = new HashMap<>();
	public static Map<Integer, Class<? extends Container>> ItemInvMapping = new HashMap<>();




	@SuppressWarnings({"unchecked"})
	public static void RegisterThings() throws ClassNotFoundException
	{
		AbstractBlock ore;
		// construct normal blocks here
		BlockList.put("OreBlock", ore = new OreBlock());
		BlockList.put("BlastFurnaceCasing", new BlockBlastFurnaceCasing());
		BlockList.put("BlastFurnaceController", new BlockBlastFurnaceController());
		BlockList.put("BasicEnergyCellFrame", new BlockBasicEnergyCellFrame());
		BlockList.put("AdvancedEnergyCellFrame", new BlockAdvancedEnergyCellFrame());
		BlockList.put("OreCrusherCasing", new BlockOreCrusherCasing());
		BlockList.put("OreCrusherController", new BlockOreCrusherController());

		// do tile entity work
		{
			// begin black magic
			// search our package classpath for all classes.
			Set<Class> classes = Utilities.GetClassesInPackage(Constants.PackageName + "." + Constants.ModID);

			for(Class cla : classes)
			{
				// check that it has the annotation we want.
				if(!cla.isAnnotationPresent(RequiresTileEntity.class))
					continue;

				// get our required fields
				RequiresTileEntity annot = (RequiresTileEntity) cla.getAnnotation(RequiresTileEntity.class);
				Class<? extends Container> containerclass = annot.ContainerClass();
				Class<? extends GuiContainer> guiclass = annot.GuiContainerClass();
				Class<? extends TileEntity> tileentityclass = annot.TileEntityClass();
				int GuiID = annot.GuiID();

				// always register the tile entity first.
				GameRegistry.registerTileEntity(tileentityclass, "TileEntity"
					+ cla.getSimpleName().substring(cla.getSimpleName().indexOf("Block")));

				// check for the rest.
				if(containerclass != RequiresTileEntity.DefaultContainer.class)
					TEInvMapping.put(GuiID, containerclass);

				if(guiclass != RequiresTileEntity.DefaultGuiContainer.class)
					ChemicalEnergy.Proxy.RegisterClientTileEntity(GuiID, guiclass);
			}

			// end black magic
		}

		{
			ItemList.put("BatteryRack", new ItemBatteryRack());
			ItemInvMapping.put(Constants.GuiIDs.BatteryRack.getID(), InventoryBatteryRack.class);
		}



		// construct items here
		ItemList.put("MetalIngot", new ItemMetalIngot());
		ItemList.put("Material", new ItemMaterial());
		ItemList.put("LeadAcidBattery", new LeadAcidBattery());
		ItemList.put("NiMHBattery", new NiMHBattery());
		ItemList.put("LithiumIonBattery", new LithiumIonBattery());
		ItemList.put("DebugItem", new DebugItem());
		CreativeTabItem = ItemList.get("LithiumIonBattery");









		for(Block gb : BlockList.values())
		{
			if(gb.getClass().isAnnotationPresent(RequiresItemBlock.class))
			{
				GameRegistry.registerBlock(gb, gb.getClass().getAnnotation(RequiresItemBlock.class).ItemBlockClass(),
					Utilities.UnwrapName(gb.getUnlocalizedName()));
			}
			else
			{
				GameRegistry.registerBlock(gb, Utilities.UnwrapName(gb.getUnlocalizedName()));
			}
		}

		for(Item gi : ItemList.values())
			GameRegistry.registerItem(gi, Utilities.UnwrapName(gi.getUnlocalizedName()));




		// oredict stuff
		for(int i = 0; i < Constants.Ores.values().length; i++)
		{
			ItemStack stack = new ItemStack(BlockList.get("OreBlock"), 1, i);
			OreDictionary.registerOre("ore" + Constants.Ores.values()[i].name(), stack);
		}

		for(int i = 0; i < Constants.MetalIngots.values().length; i++)
		{
			OreDictionary.registerOre("ingot" + Constants.MetalIngots.values()[i].name(), new ItemStack(ItemMetalIngot.Instance, 1, i));
		}

		for(int i = 0; i < Constants.Materials.values().length; i++)
		{
			OreDictionary.registerOre("dust" + Constants.Materials.values()[i].GetOreDictName(),
				new ItemStack(ItemMaterial.Instance, 1, i));
		}
	}

	public static void RegisterOreDictionaryThings()
	{
		// call after everything is done
		// shove carbon things into oredict
		for(ItemStack stack : OreDictionary.getOres("treeSapling"))
			Constants.CarbonItems.put(stack.getItem(), 1);

		for(ItemStack stack : OreDictionary.getOres("treeLeaves"))
			Constants.CarbonItems.put(stack.getItem(), 2);

		for(ItemStack stack : OreDictionary.getOres("logWood"))
			Constants.CarbonItems.put(stack.getItem(), 64);

		for(ItemStack stack : OreDictionary.getOres("plankWood"))
			Constants.CarbonItems.put(stack.getItem(), 16);

		for(ItemStack stack : OreDictionary.getOres("stickWood"))
			Constants.CarbonItems.put(stack.getItem(), 2);

		Constants.CarbonItems.put(Items.coal, 256);
	}

	public static void RegisterThingsStage2()
	{
		RegisterCustomRecipes();
		NetworkRegistry.INSTANCE.registerGuiHandler(ChemicalEnergy.Instance, new GlobalGUIHandler());
	}

	public static void RegisterCustomRecipes()
	{
		Map<Integer, Double> PurificationWeights = new HashMap<>();
		PurificationWeights.put(1, 0.25);
		PurificationWeights.put(2, 0.75);

		Map<Integer, Double> SmeltingWeights = new HashMap<>();
		SmeltingWeights.put(1, 0.75);
		SmeltingWeights.put(2, 0.25);


		// Blast Furnace
		BlastFurnaceRecipes.clear();
		for(ItemStack stack : OreDictionary.getOres("dustImpureChalcopyrite"))
			BlastFurnaceRecipes.add(new MachineRecipe(stack,
				new ItemStack(ItemMaterial.Instance, 1, Constants.Materials.GetMetadataFromName("CopperSulphideDust"))));

		for(ItemStack stack : OreDictionary.getOres("dustChalcopyrite"))
			BlastFurnaceRecipes.add(new MachineRecipe(stack,
				new ItemStack(ItemMaterial.Instance, 1, Constants.Materials.GetMetadataFromName("CopperSulphideDust")), SmeltingWeights));




		for(ItemStack stack : OreDictionary.getOres("dustImpureCassiterite"))
			BlastFurnaceRecipes.add(new MachineRecipe(stack,
				new ItemStack(ItemMetalIngot.Instance, 1, Constants.MetalIngots.GetMetadataFromName("Tin")), Constants.CatalystTypes.Carbon, true, 50));

		for(ItemStack stack : OreDictionary.getOres("dustCassiterite"))
			BlastFurnaceRecipes.add(new MachineRecipe(stack,
				new ItemStack(ItemMetalIngot.Instance, 1, Constants.MetalIngots.GetMetadataFromName("Tin")), Constants.CatalystTypes.Carbon, true, 50,
				SmeltingWeights));
	}
}














