// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Handlers;

import cpw.mods.fml.common.network.IGuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;

public class GlobalGUIHandler implements IGuiHandler
{
	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		if(CentralRegistry.TEInvMapping.get(ID) != null)
		{
			try
			{
				return CentralRegistry.TEInvMapping.get(ID).getConstructor(InventoryPlayer.class, TileEntity.class).newInstance(player.inventory,
					world.getTileEntity(x, y, z));
			}
			catch(Exception e)
			{
				ChemicalEnergy.LogObject.log(Level.FATAL, String.format("%s", e.getClass().getSimpleName()));
			}
		}
		else if(CentralRegistry.ItemInvMapping.get(ID) != null)
		{
			try
			{
				return CentralRegistry.ItemInvMapping.get(ID).getConstructor(InventoryPlayer.class, ItemStack.class).newInstance(player.inventory,
					player.getHeldItem());
			}
			catch(Exception e)
			{
				ChemicalEnergy.LogObject.log(Level.FATAL, String.format("%s", e.getClass().getSimpleName()));
			}
		}

		ChemicalEnergy.LogObject.log(Level.FATAL, String.format("Tried to activate Inventory id %d, ignoring...", ID));
		return null;
	}




	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		if(CentralRegistry.TEGuiMapping.get(ID) != null)
		{
			try
			{
				return CentralRegistry.TEGuiMapping.get(ID).getConstructor(InventoryPlayer.class, TileEntity.class).newInstance(player.inventory,
					world.getTileEntity(x, y, z));
			}
			catch(Exception e)
			{
				ChemicalEnergy.LogObject.log(Level.FATAL, String.format("%s", e.getClass().getSimpleName()));
			}
		}
		else if(CentralRegistry.ItemGuiMapping.get(ID) != null)
		{
			try
			{
				return CentralRegistry.ItemGuiMapping.get(ID).getConstructor(InventoryPlayer.class, ItemStack.class).newInstance(player.inventory,
					player.getHeldItem());
			}
			catch(Exception e)
			{
				ChemicalEnergy.LogObject.log(Level.FATAL, String.format("%s", e.getClass().getSimpleName()));
			}
		}

		ChemicalEnergy.LogObject.log(Level.FATAL, String.format("Tried to activate GUI id %d, ignoring...", ID));
		return null;
	}
}
