// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Handlers;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import net.minecraft.client.Minecraft;

public class RenderTickHandler
{
	private Minecraft minecraft;

	public RenderTickHandler(Minecraft mc)
	{
		this.minecraft = mc;
	}

	@SubscribeEvent
	public void RenderTick(TickEvent.RenderTickEvent event)
	{
		if(this.minecraft.thePlayer == null)
			return;

		this.minecraft.thePlayer.motionY += 0.00f;
	}
}
