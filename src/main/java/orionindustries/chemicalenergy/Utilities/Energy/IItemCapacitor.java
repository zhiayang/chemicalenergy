// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Energy;

import net.minecraft.item.ItemStack;

public interface IItemCapacitor
{
	// Should return the currently stored energy in the item/block/thing, in RF.
	public abstract int GetStoredEnergy(ItemStack stack);

	// Should return the maximum amount of energy that can be stored, in RF.
	public abstract int GetMaxEnergy(ItemStack stack);

	// Should store 'Energy' RF into storage, returning the remainder that could not be stored.
	public abstract int StoreEnergy(ItemStack stack, int Energy);

	// Should subtract 'Energy' RF from storage, returning the actual amount of RF gotten.
	public abstract int ExtractEnergy(ItemStack stack, int Energy);

	public abstract int GetTransferRate(ItemStack stack);
	public abstract void SetMaxEnergy(ItemStack stack,  int MaxEnergy);
	public abstract void SetTransferRate(ItemStack stack,  int TransferRate);
	public abstract boolean GetRechargable(ItemStack stack);
}
