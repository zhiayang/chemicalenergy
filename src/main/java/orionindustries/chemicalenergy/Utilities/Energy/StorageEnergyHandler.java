// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Energy;

import buildcraft.api.power.IPowerReceptor;
import buildcraft.api.power.PowerHandler;
import cofh.api.energy.EnergyStorage;
import net.minecraft.tileentity.TileEntity;
import orionindustries.chemicalenergy.Utilities.Constants;

public class StorageEnergyHandler extends GenericEnergyHandler
{
	public StorageEnergyHandler(int capacity, int MaxMJInput, IPowerReceptor t)
	{
		super(new EnergyStorage(capacity), new PowerHandler(t, PowerHandler.Type.STORAGE),
				(t instanceof TileEntity ? (TileEntity) t : null));

		this.MJStorage.configure(2, MaxMJInput, 0.01, capacity / Constants.RFToMJRatio);
	}
}
