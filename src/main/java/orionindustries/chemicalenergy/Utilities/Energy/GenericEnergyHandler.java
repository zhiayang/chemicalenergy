// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Energy;

import buildcraft.api.power.PowerHandler;
import cofh.api.energy.EnergyStorage;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import orionindustries.chemicalenergy.Utilities.Constants;



public abstract class GenericEnergyHandler
{
	protected EnergyStorage RFStorage;
	protected PowerHandler MJStorage;
	protected TileEntity tile;

	/**
	 * @param rf RF EnergyStorage, initialised with the desired values.
	 * @param mj BC PowerHandler, initialised and configured with values.
	 * @param t  TileEntity reference, for automatically marking block for update.
	 *           <p/>
	 *           Note that this class will use RF storage as the default system, meaning that it will
	 *           convert any MJ power input into RF.
	 */
	public GenericEnergyHandler(EnergyStorage rf, PowerHandler mj, TileEntity t)
	{
		this.RFStorage = rf;
		this.MJStorage = mj;
		this.tile = t;
	}

	/**
	 * @param rf RF EnergyStorage, initialised with the desired values.
	 * @param mj BC PowerHandler, initialised and configured with values.
	 *           <p/>
	 *           Note that this class will use RF storage as the default system, meaning that it will
	 *           convert any MJ power input into RF.
	 */
	public GenericEnergyHandler(EnergyStorage rf, PowerHandler mj)
	{
		this(rf, mj, null);
	}



	/**
	 *
	 * @param newEnergy the new amount of energy
	 *
	 * Do not call this method under normal circumstances. Ever.
	 */
	public void DirectlyModifyEnergy(int newEnergy)
	{
		this.RFStorage.setEnergyStored(newEnergy);
	}


	public int GetStoredEnergy()
	{
		return this.RFStorage.getEnergyStored();
	}

	public int GetMaxEnergy()
	{
		return this.RFStorage.getMaxEnergyStored();
	}

	public void SetMaxEnergy(int Energy)
	{
		this.RFStorage.setCapacity(Energy);
	}

	public int GetTransferRate()
	{
		return this.RFStorage.getMaxExtract();
	}

	public void SetTransferRate(int rate)
	{
		this.RFStorage.setMaxTransfer(rate);
	}


	public int StoreEnergy(int Energy)
	{
		return this.StoreEnergy(Energy, false);
	}

	public int ExtractEnergy(int Energy)
	{
		return this.ExtractEnergy(Energy, false);
	}

	public int StoreEnergy(int Energy, boolean sim)
	{
//		if(this.tile != null)
//			this.tile.getWorldObj().markBlockForUpdate(this.tile.xCoord, this.tile.yCoord, this.tile.zCoord);

		return this.RFStorage.receiveEnergy(Energy, sim);
	}

	public int ExtractEnergy(int Energy, boolean sim)
	{
//		if(this.tile != null)
//			this.tile.getWorldObj().markBlockForUpdate(this.tile.xCoord, this.tile.yCoord, this.tile.zCoord);

		return this.RFStorage.extractEnergy(Energy, sim);
	}




	// NBT stuff
	public void WriteToNBT(NBTTagCompound tag)
	{
		this.RFStorage.writeToNBT(tag);
		this.MJStorage.writeToNBT(tag);
	}

	public void ReadFromNBT(NBTTagCompound tag)
	{
		this.RFStorage.readFromNBT(tag);
		this.MJStorage.readFromNBT(tag);
	}




	// BC compatibility layer.
	public PowerHandler.PowerReceiver GetBCPowerHandler()
	{
		return this.MJStorage.getPowerReceiver();
	}


	public void BCDoWork()
	{
		double used = this.MJStorage.getEnergyStored();
		this.MJStorage.useEnergy(used, used, true);

		this.StoreEnergy((int) used * Constants.RFToMJRatio);
	}
}
