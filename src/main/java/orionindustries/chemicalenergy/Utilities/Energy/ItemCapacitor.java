// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Energy;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import orionindustries.chemicalenergy.ChemicalEnergy;

public class ItemCapacitor extends Item implements IItemCapacitor, IEnergyContainerItem
{
	protected int capacity;
	protected int maxReceive;
	protected int maxExtract;
	protected boolean rechargable;

	public ItemCapacitor()
	{
		super();
	}

	public ItemCapacitor(int capacity, boolean rechargable)
	{
		this(capacity, capacity, capacity, rechargable);
	}

	public ItemCapacitor(int capacity, int maxTransfer, boolean rechargable)
	{
		this(capacity, maxTransfer, maxTransfer, rechargable);
	}

	public ItemCapacitor(int capacity, int maxReceive, int maxExtract, boolean recharge)
	{
		this.capacity = capacity;
		this.maxReceive = maxReceive;
		this.maxExtract = maxExtract;
		this.rechargable = recharge;
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
	}

	private void SetupNBT(ItemStack stack)
	{
		if(stack == null)
			return;

		if(!stack.hasTagCompound())
		{
			stack.stackTagCompound = new NBTTagCompound();
			stack.stackTagCompound.setInteger("MaxEnergy", this.capacity);
			stack.stackTagCompound.setInteger("TransferRate", this.maxExtract);
		}
	}



	@Override
	public int receiveEnergy(ItemStack container, int maxReceive, boolean simulate)
	{
		if(!this.rechargable)
			return 0;

		this.SetupNBT(container);
		if(container.stackTagCompound == null)
		{
			container.stackTagCompound = new NBTTagCompound();
		}
		int energy = container.stackTagCompound.getInteger("Energy");
		int energyReceived = Math.min(this.GetMaxEnergy(container) - energy, Math.min(this.GetTransferRate(container), maxReceive));

		if(!simulate)
		{
			energy += energyReceived;
			container.stackTagCompound.setInteger("Energy", energy);
		}
		return energyReceived;
	}

	@Override
	public int extractEnergy(ItemStack container, int maxExtract, boolean simulate)
	{
		this.SetupNBT(container);
		if(container.stackTagCompound == null || !container.stackTagCompound.hasKey("Energy"))
		{
			return 0;
		}
		int energy = container.stackTagCompound.getInteger("Energy");
		int energyExtracted = Math.min(energy, Math.min(this.GetTransferRate(container) - 10, maxExtract));

		if(!simulate)
		{
			energy -= energyExtracted;
			container.stackTagCompound.setInteger("Energy", energy);
		}
		return energyExtracted;
	}

	@Override
	public int getEnergyStored(ItemStack container)
	{
		this.SetupNBT(container);
		if(container == null || container.stackTagCompound == null || !container.stackTagCompound.hasKey("Energy"))
		{
			return 0;
		}
		return container.stackTagCompound.getInteger("Energy");
	}

	@Override
	public int getMaxEnergyStored(ItemStack container)
	{
		return this.GetMaxEnergy(container);
	}





	@Override
	public int GetStoredEnergy(ItemStack stack)
	{
		return this.getEnergyStored(stack);
	}

	@Override
	public int StoreEnergy(ItemStack stack, int Energy)
	{
		return this.receiveEnergy(stack, Energy, false);
	}

	@Override
	public int ExtractEnergy(ItemStack stack, int Energy)
	{
		return this.extractEnergy(stack, Energy, false);
	}

	public void ReadFromNBT(ItemStack stack)
	{
		this.SetupNBT(stack);
	}

	public void WriteToNBT(ItemStack stack)
	{
		this.SetupNBT(stack);
	}


	@Override
	public int GetTransferRate(ItemStack stack)
	{
		this.SetupNBT(stack);
		return stack.stackTagCompound.getInteger("TransferRate");
	}

	@Override
	public int GetMaxEnergy(ItemStack stack)
	{
		this.SetupNBT(stack);
		return stack.stackTagCompound.getInteger("MaxEnergy");
	}

	@Override
	public void SetMaxEnergy(ItemStack stack, int MaxEnergy)
	{
		this.SetupNBT(stack);
		stack.stackTagCompound.setInteger("MaxEnergy", MaxEnergy);
	}

	@Override
	public void SetTransferRate(ItemStack stack, int TransferRate)
	{
		this.SetupNBT(stack);
		stack.stackTagCompound.setInteger("TransferRate", TransferRate);
	}

	@Override
	public boolean GetRechargable(ItemStack stack)
	{
		return this.rechargable;
	}
}
