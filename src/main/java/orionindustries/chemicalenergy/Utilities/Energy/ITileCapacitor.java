// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Energy;

public abstract interface ITileCapacitor
{
	// Should return the currently stored energy in the item/block/thing, in RF.
	public abstract int GetStoredEnergy();

	// Should return the maximum amount of energy that can be stored, in RF.
	public abstract int GetMaxEnergy();

	// Should store 'Energy' RF into storage, returning the remainder that could not be stored.
	public abstract int StoreEnergy(int Energy);

	// Should subtract 'Energy' RF from storage, returning the actual amount of RF gotten.
	public abstract int ExtractEnergy(int Energy);

	public abstract GenericEnergyHandler GetEnergyHandler();
}
