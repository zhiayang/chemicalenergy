// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities;

import net.minecraft.block.Block;
import net.minecraft.world.World;

import javax.vecmath.Point3d;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MultiblockStructureDefinition
{
	private String[][] Structure;
	private Map<Character, List<Block>> Blocks;

	/**
	 * @param struct   the multiblock structure, bottom-up
	 * @param mappings a map of each character used in struct to the block to check
	 */
	public MultiblockStructureDefinition(String[][] struct, Map<Character, List<Block>> mappings)
	{
		this.Structure = struct;
		this.Blocks = mappings;
	}


	private boolean BlockMatches(World world, int x, int y, int z, int ax, int ay, int az)
	{
		return !(this.Blocks.get(this.Structure[ay][ax].charAt(az)) == null ||
				!this.Blocks.get(this.Structure[ay][ax].charAt(az)).contains(world.getBlock(x, y, z)));
	}

	/**
	 * @param world the world
	 * @param bx    the x-coordinate of the bottom centre block
	 * @param by    the y-coordinate of the bottom centre block
	 * @param bz    the z-coordinate of the bottom centre block
	 * @return whether or not the structure is valid
	 */
	public boolean CheckStructure(World world, int bx, int by, int bz)
	{
		/*
			for structure

			1 2 3		a b c		j k l
			4 5 6	->	d e f	->	m n o
			7 8 9		g h i		p q r

			assume (x, y, z) = 1.
			so apparently z+ is back, x+ is right.
		 */

		// { { "###" , "###" , "###" }, { "***", "***", "***" }, { "@@@", "@@@", "@@@" } }

		String[][] Or1 = new String[this.Structure.length][this.Structure[0].length];
		String[][] Or2 = new String[this.Structure.length][this.Structure[0].length];
		String[][] Or3 = new String[this.Structure.length][this.Structure[0].length];
		String[][] Or4 = new String[this.Structure.length][this.Structure[0].length];


		for(int i = 0; i < this.Structure.length; i++)
			for(int j = 0; j < this.Structure[i].length; j++)
				Or1[i][j] = this.Structure[i][j];

		for(int i = 0; i < this.Structure.length; i++)
			for(int j = 0; j < this.Structure[i].length; j++)
				Or2[i][j] = this.Structure[i][j];

		for(int i = 0; i < this.Structure.length; i++)
			for(int j = 0; j < this.Structure[i].length; j++)
				Or3[i][j] = this.Structure[i][j];

		for(int i = 0; i < this.Structure.length; i++)
			for(int j = 0; j < this.Structure[i].length; j++)
				Or4[i][j] = this.Structure[i][j];


		// Or1: swap first and third of second level { { "aaa", "bbb", "ccc" } } = { { "ccc", "bbb", "aaa" } }
		// Or2: swap first and third of second level, do string manip to swap first char with last char of second level
		// Or3: swap chars and strings
		// Or4: don't swap.

		int width = this.Structure[0].length;

		// Or1.
		for(int i = 0; i < Or1.length; i++)
		{
			int n = Or1[i].length;
			String temp = Or1[i][0];

			Or1[i][0] = Or1[i][n - 1];
			Or1[i][n - 1] = temp;
		}


		// Or2.
		for(int i = 0; i < Or2.length; i++)
		{
			char[][] layer = new char[width][width];
			char[][] orig = new char[width][width];

			for(int r = 0; r < width; r++)
				for(int u = 0; u < width; u++)
					orig[r][u] = this.Structure[i][r].charAt(u);


			for(int u = 0; u < width; u++)
				for(int k = 0; k < width; k++)
					layer[k][u] = orig[u][width - 1 - k];


			for(int r = 0; r < width; r++)
				Or2[i][r] = new String(layer[r]);

		}



		// Or3.
		for(int i = 0; i < Or3.length; i++)
		{
			char[][] layer = new char[width][width];
			char[][] orig = new char[width][width];

			for(int r = 0; r < width; r++)
				for(int u = 0; u < width; u++)
					orig[width - 1 - r][u] = this.Structure[i][r].charAt(u);


			for(int u = 0; u < width; u++)
				for(int k = 0; k < width; k++)
					layer[k][u] = orig[u][k];

			for(int r = 0; r < width; r++)
				Or3[i][r] = new String(layer[r]);
		}










		boolean o1 = this.CheckStructure(world, bx, by, bz, Or1);
		boolean o2 = this.CheckStructure(world, bx, by, bz, Or2);
		boolean o3 = this.CheckStructure(world, bx, by, bz, Or3);
		boolean o4 = this.CheckStructure(world, bx, by, bz, Or4);

		return o1 || o2 || o3 || o4;
	}

	/**
	 *
	 * @param bx centremost bottom block, x-coord
	 * @param by centremost bottom block, y-coord
	 * @param bz centremost bottom block, z-coord
	 * @return a list of Point3Ds of the block coords.
	 */

	public List<Point3d> GetBlocksOfStructure(int bx, int by, int bz)
	{
		int height = this.Structure.length;
		int width = this.Structure[0].length;
		int length = this.Structure[0][0].length();

		List<Point3d> ret = new ArrayList<>();

		for(int y = by, cy = 0; cy < height; y++, cy++)
		{
			for(int x = bx - (width / 2), cx = 0; cx < width; x++, cx++)
			{
				for(int z = bz - (width / 2), cz = 0; cz < length; z++, cz++)
				{
					ret.add(new Point3d(x, y, z));
				}
			}
		}

		return ret;
	}

	private boolean CheckStructure(World world, int bx, int by, int bz, String[][] structure)
	{
		int height = structure.length;
		int width = structure[0].length;
		int length = structure[0][0].length();


		for(int y = by, cy = 0; cy < height; y++, cy++)
		{
			for(int x = bx - 1, cx = 0; cx < width; x++, cx++)
			{
				for(int z = bz - 1, cz = 0; cz < length; z++, cz++)
				{
					Block block = world.getBlock(x, y, z);
					if(this.Blocks.get(structure[cy][cx].charAt(cz)) == null ||
							!this.Blocks.get(structure[cy][cx].charAt(cz)).contains(block))
					{
						return false;
					}
				}
			}
		}

		return true;
	}
}
