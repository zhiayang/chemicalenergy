// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Slots;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack.ItemBatteryRack;
import orionindustries.chemicalenergy.Items.Energy.Batteries.LeadAcidBattery;

public class FEEnergyDeviceSlot extends Slot
{
	public FEEnergyDeviceSlot(IInventory inv, int slotindex, int x, int y)
	{
		super(inv, slotindex, x, y);
	}

	@Override
	public boolean isItemValid(ItemStack stack)
	{
		return stack.getItem() instanceof ItemBatteryRack || stack.getItem() instanceof LeadAcidBattery;
	}
}
