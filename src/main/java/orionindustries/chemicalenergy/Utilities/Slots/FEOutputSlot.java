// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Slots;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class FEOutputSlot extends FESlot
{
	public FEOutputSlot(IInventory inv, int slotindex, int x, int y)
	{
		super(inv, slotindex, x, y);
	}

	@Override
	public boolean isItemValid(ItemStack par1ItemStack)
	{
		return false;
	}
}
