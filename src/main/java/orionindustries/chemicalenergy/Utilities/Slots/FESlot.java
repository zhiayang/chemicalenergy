// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Utilities.Slots;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class FESlot extends Slot
{
	public FESlot(IInventory inv, int slotindex, int x, int y)
	{
		super(inv, slotindex, x, y);
	}

	@Override
	public boolean isItemValid(ItemStack par1ItemStack)
	{
		return true;
	}
}
