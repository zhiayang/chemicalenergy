// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import orionindustries.chemicalenergy.Utilities.Constants;

public class GenericDryCell extends GenericBattery
{
	private IIcon icon;
	private IIcon[] levels;

	private String GaugeColour;



	public static GenericBattery Instance;

	public GenericDryCell(int capacity, int transferrate, boolean Rechargable, boolean WetCell, String gauge)
	{
		super(capacity, transferrate, Rechargable, WetCell);
		this.GaugeColour = gauge;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "item." + Constants.ModID + ".NiMHBattery";
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return this.getUnlocalizedName();
	}

	@Override
	public void registerIcons(IIconRegister iir)
	{
		this.icon = iir.registerIcon(Constants.ResourcePrefix + "Batteries/DryCell");
		this.levels = new IIcon[Constants.BatteryLevels];
		this.levels[0] = iir.registerIcon(Constants.ResourcePrefix + "Batteries/Levels/0");

		for(int i = 1; i < Constants.BatteryLevels; i++)
		{
			this.levels[i] = iir.registerIcon(Constants.ResourcePrefix + "Batteries/Levels/" + this.GaugeColour + "/" + String.valueOf(i));
		}
	}

	@Override
	public int getRenderPasses(int metadata)
	{
		return 2;
	}

	@Override
	public boolean requiresMultipleRenderPasses()
	{
		return true;
	}

	@Override
	public IIcon getIcon(ItemStack stack, int pass)
	{
		if(pass == 0)
			return this.icon;

		else
		{
			double totale = GenericBattery.Instance.GetMaxEnergy(stack);
			double actuale = GenericBattery.Instance.GetStoredEnergy(stack);

			int num = (int) ((actuale / totale) * 20);
			return this.levels[num];
		}
	}
}
