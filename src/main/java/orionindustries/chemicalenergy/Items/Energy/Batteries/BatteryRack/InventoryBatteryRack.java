// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;
import orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery.GenericBattery;
import orionindustries.chemicalenergy.Utilities.Slots.FEDryCellSlot;

public class InventoryBatteryRack extends Container implements IInventory
{
	public static int InventorySize = 6;
	public ItemStack[] Inventory = new ItemStack[InventorySize];
	private final ItemStack stack;
	private final InventoryPlayer inventoryPlayer;

	private static final int INV_START = InventoryBatteryRack.InventorySize;
	private static final int INV_END = INV_START + 26;
	private static final int HOTBAR_START = INV_END + 1;
	private static final int HOTBAR_END = HOTBAR_START + 8;


	public InventoryBatteryRack(InventoryPlayer playerinv, ItemStack itemStack)
	{
		this.stack = itemStack;
		this.inventoryPlayer = playerinv;

		if(!this.stack.hasTagCompound())
		{
			this.stack.setTagCompound(new NBTTagCompound());
		}


		this.addSlotToContainer(new FEDryCellSlot(this, 0, 61 + 37, 25));
		this.addSlotToContainer(new FEDryCellSlot(this, 1, 79 + 37, 25));
		this.addSlotToContainer(new FEDryCellSlot(this, 2, 97 + 37, 25));
		this.addSlotToContainer(new FEDryCellSlot(this, 3, 61 + 37, 43));
		this.addSlotToContainer(new FEDryCellSlot(this, 4, 79 + 37, 43));
		this.addSlotToContainer(new FEDryCellSlot(this, 5, 97 + 37, 43));

		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 9; j++)
			{
				this.addSlotToContainer(new Slot(inventoryPlayer, j + (i * 9) + 9, 8 + (j * 18), 79 + (i * 18)));
			}
		}

		for(int i = 0; i < 9; ++i)
			this.addSlotToContainer(new Slot(inventoryPlayer, i, 8 + (i * 18), 137));



		this.readFromNBT(this.stack.stackTagCompound);
	}


	/**
	 * Called when a player shift-clicks on a slot. You must override this or you will crash when someone does that.
	 * Only real change we make to this is to set needsUpdate to true at the end
	 */
	public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slotnum)
	{
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(slotnum);

		if(slot != null && slot.getHasStack())
		{
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			// Either armor or sharingan eye
			if(slotnum < INV_START)
			{
				// try to place in player inventory / action bar
				if(!this.mergeItemStack(itemstack1, INV_START, HOTBAR_END + 1, true))
				{
					return null;
				}

				slot.onSlotChange(itemstack1, itemstack);
			}
			// Item is in inventory / hotbar, try to place either in eye or armor slots
			else
			{
				// if item is a sharingan eye
				if(GenericBattery.IsDryCell(itemstack1))
				{
					if(!this.mergeItemStack(itemstack1, 0, INV_START, false))
					{
						return null;
					}
				}

				// item in player's inventory, but not in action bar
				else if(slotnum >= INV_START && slotnum < HOTBAR_START)
				{
					// place in action bar
					if(!this.mergeItemStack(itemstack1, HOTBAR_START, HOTBAR_START + 1, false))
					{
						return null;
					}
				}
				// item in action bar - place in player inventory
				else if(slotnum >= HOTBAR_START && slotnum < HOTBAR_END + 1)
				{
					if(!this.mergeItemStack(itemstack1, INV_START, INV_END + 1, false))
					{
						return null;
					}
				}
			}

			if(itemstack1.stackSize == 0)
			{
				slot.putStack(null);
			}
			else
			{
				slot.onSlotChanged();
			}

			if(itemstack1.stackSize == itemstack.stackSize)
			{
				return null;
			}

			slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
		}

		return itemstack;
	}

	@Override
	public ItemStack slotClick(int slot, int button, int flag, EntityPlayer player)
	{
		if(slot >= 0 && this.getSlot(slot) != null && this.getSlot(slot).getStack() == player.getHeldItem())
		{
			return null;
		}
		return super.slotClick(slot, button, flag, player);
	}

	@Override
	public int getSizeInventory()
	{
		return InventorySize;
	}

	@Override
	public ItemStack getStackInSlot(int var1)
	{
		return this.Inventory[var1];
	}

	@Override
	public ItemStack decrStackSize(int SlotIndex, int Decr)
	{
		ItemStack stack = this.getStackInSlot(SlotIndex);
		if(stack != null)
		{
			if(stack.stackSize <= Decr)
			{
				this.setInventorySlotContents(SlotIndex, null);
			}
			else
			{
				stack = stack.splitStack(Decr);
				if(stack.stackSize == 0)
				{
					this.setInventorySlotContents(SlotIndex, null);
				}
			}
		}
		return stack;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int var1)
	{
		ItemStack stack = getStackInSlot(var1);
		if(stack != null)
		{
			this.setInventorySlotContents(var1, null);
		}
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack var2)
	{
		this.Inventory[index] = var2;

		if(var2 != null && var2.stackSize > this.getInventoryStackLimit())
		{
			var2.stackSize = this.getInventoryStackLimit();
		}
	}

	@Override
	public String getInventoryName()
	{
		return "Battery Rack";
	}

	@Override
	public boolean hasCustomInventoryName()
	{
		return true;
	}

	@Override
	public int getInventoryStackLimit()
	{
		return 64;
	}

	@Override
	public void markDirty()
	{
		NBTTagCompound tagCompound = new NBTTagCompound();
		this.writeToNBT(tagCompound);
		this.stack.setTagCompound(tagCompound);

		System.err.println("fuck you");
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer var1)
	{
		return ItemStack.areItemStacksEqual(var1.getHeldItem(), this.stack);
	}

	@Override
	public void openInventory()
	{
		this.readFromNBT(this.stack.stackTagCompound);
	}

	@Override
	public void closeInventory()
	{
		this.markDirty();
	}

	@Override
	public boolean isItemValidForSlot(int var1, ItemStack var2)
	{
		return true;
	}

	public void readFromNBT(NBTTagCompound tagcompound)
	{
		NBTTagList items = tagcompound.getTagList("Items", Constants.NBT.TAG_COMPOUND);

		for(int i = 0; i < items.tagCount(); ++i)
		{
			NBTTagCompound item = items.getCompoundTagAt(i);
			byte slot = item.getByte("Slot");

			if(slot >= 0 && slot < getSizeInventory())
			{
				this.Inventory[slot] = ItemStack.loadItemStackFromNBT(item);
			}
		}
	}

	public void writeToNBT(NBTTagCompound tagcompound)
	{
		NBTTagList items = new NBTTagList();

		for(int i = 0; i < getSizeInventory(); ++i)
		{
			if(this.getStackInSlot(i) != null)
			{
				NBTTagCompound item = new NBTTagCompound();
				item.setByte("Slot", (byte) i);
				this.getStackInSlot(i).writeToNBT(item);

				items.appendTag(item);
			}
		}
		tagcompound.setTag("Items", items);
	}

	@Override
	public boolean canInteractWith(EntityPlayer var1)
	{
		return this.isUseableByPlayer(var1);
	}
}
