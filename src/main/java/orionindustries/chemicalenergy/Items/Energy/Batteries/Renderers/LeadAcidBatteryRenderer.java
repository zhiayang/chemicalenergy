// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries.Renderers;

import cpw.mods.fml.client.FMLClientHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;
import org.lwjgl.opengl.GL11;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Render.Model.WavefrontObj.Object3D;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LeadAcidBatteryRenderer implements IItemRenderer
{
	private Object3D WetCellModel;

	public LeadAcidBatteryRenderer()
	{
		try
		{
			this.WetCellModel = new Object3D(new BufferedReader(new InputStreamReader(Minecraft.getMinecraft().getResourceManager()
				.getResource(Constants.Resources.WetCellModel.GetResource()).getInputStream())), true);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public boolean handleRenderType(ItemStack item, ItemRenderType type)
	{
		return true;
	}

	@Override
	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper)
	{
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data)
	{
		GL11.glPushMatrix();

		FMLClientHandler.instance().getClient().getTextureManager()
			.bindTexture(Constants.Resources.WetCellTexture.GetResource());

		GL11.glScaled(0.015, 0.015, 0.015);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glColor4d(1.0, 1.0, 1.0, 1.0);

		if(type == ItemRenderType.EQUIPPED_FIRST_PERSON || type == ItemRenderType.EQUIPPED_FIRST_PERSON)
		{
			GL11.glScaled(1.25, 1.25, 1.25);
			GL11.glTranslated(40, 30, 30);
			GL11.glRotated(45, 0, 1, 0);
		}

		this.WetCellModel.Render();

		GL11.glPopMatrix();
	}
}
