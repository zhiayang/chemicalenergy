// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery.GenericBattery;
import orionindustries.chemicalenergy.Utilities.Constants;

public class LeadAcidBattery extends GenericBattery
{
	private IIcon icon;
	public static GenericBattery Instance;

	public LeadAcidBattery()
	{
		super(Constants.BatteryValues.LeadAcidRechargable.MaxEnergy(),
				Constants.BatteryValues.LeadAcidRechargable.TransferRate(), true, true);

		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
		Instance = this;
	}


	@Override
	public String getUnlocalizedName()
	{
		return "item." + Constants.ModID + ".LeadAcidBattery";
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return this.getUnlocalizedName();
	}

	@Override
	public void registerIcons(IIconRegister iir)
	{
		this.icon = iir.registerIcon(Constants.ResourcePrefix + "Batteries/LeadAcid");
	}

	@Override
	public IIcon getIconFromDamage(int par1)
	{
		return this.icon;
	}

	@Override
	public void SetMaxEnergy(ItemStack stack, int MaxEnergy)
	{
	}

	@Override
	public void SetTransferRate(ItemStack stack, int TransferRate)
	{
	}
}
