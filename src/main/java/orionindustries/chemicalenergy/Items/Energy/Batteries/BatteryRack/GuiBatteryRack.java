// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack;

import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Render.Gui.GuiUtilities;
import orionindustries.chemicalenergy.Utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

public class GuiBatteryRack extends GuiContainer
{
	private final int xSize = 176;
	private final int ySize = 157;

	private ItemStack stack;

	public GuiBatteryRack(InventoryPlayer inventoryPlayer, ItemStack itemStack)
	{
		super(new InventoryBatteryRack(inventoryPlayer, itemStack));
		this.stack = itemStack;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		// Bind Texture
		this.mc.getTextureManager().bindTexture(Constants.Resources.BatteryRackGui.GetResource());

		par2 = (this.width - this.xSize) / 2;
		int j = (this.height - this.ySize) / 2;

		// draw the texture
		this.drawTexturedModalRect(par2, j, 0, 0, this.xSize, this.ySize);

		double totale = ItemBatteryRack.Instance.GetMaxEnergy(this.stack);
		double actuale = ItemBatteryRack.Instance.GetStoredEnergy(this.stack);

		double barheight = 40.0;
		double height = (actuale / totale) * barheight;

		GuiUtilities.RenderEnergyBar(this, par2 + 65, j + 18 + (int) barheight - (int) height, (int) height);
	}

	@Override
	public void drawScreen(int mx, int my, float par3)
	{
		super.drawScreen(mx, my, par3);

		GL11.glColor4d(1.0, 1.0, 1.0, 1.0);
		GL11.glDisable(GL11.GL_LIGHTING);

		int x = (this.width - this.xSize) / 2;
		int y = (this.height - this.ySize) / 2;

		int capacity = ItemBatteryRack.Instance.GetMaxEnergy(this.stack);
		int stored = ItemBatteryRack.Instance.GetStoredEnergy(this.stack);


		if(mx >= x + 65 && my >= y + 18 && mx <= x + 65 + 16 && my <= y + 18 + 40)
		{
			List<String> list = new ArrayList<>();

			String dispse = Utilities.FormatNumber(stored, false);
			String dispme = Utilities.FormatNumber(capacity);

			list.add(String.format("%sRF", dispse));
			list.add(String.format("§7%sRF§f", dispme));

			this.drawHoveringText(list, mx, my, this.fontRendererObj);
		}
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
}
