// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery.GenericBattery;
import orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery.GenericDryCell;
import orionindustries.chemicalenergy.Utilities.Constants;

import java.util.List;

public class NiMHBattery extends GenericDryCell
{
	public static GenericBattery Instance;

	public NiMHBattery()
	{
		super(Constants.BatteryValues.NickelMetalHydrideRechargable.MaxEnergy(),
				Constants.BatteryValues.NickelMetalHydrideRechargable.TransferRate(), true, false, "Black");

		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
		Instance = this;
	}


	@Override
	public String getUnlocalizedName()
	{
		return "item." + Constants.ModID + ".NiMHBattery";
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return this.getUnlocalizedName();
	}

	@Override
	@SuppressWarnings({ "unchecked" })
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		ItemStack stackfull = new ItemStack(item, 1);
		ItemStack stackempty = new ItemStack(item, 1);

		GenericBattery.Instance.SetMaxEnergy(stackempty, Constants.BatteryValues.NickelMetalHydrideRechargable.MaxEnergy());

		while(this.GetStoredEnergy(stackfull) < this.GetMaxEnergy(stackfull))
			this.StoreEnergy(stackfull,this.GetTransferRate(stackfull));

		list.add(stackempty);
		list.add(stackfull);
	}
}
