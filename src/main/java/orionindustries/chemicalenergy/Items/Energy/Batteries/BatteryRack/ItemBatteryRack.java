// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries.BatteryRack;

import cofh.api.energy.IEnergyContainerItem;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery.GenericBattery;
import orionindustries.chemicalenergy.Utilities.Constants;
import orionindustries.chemicalenergy.Utilities.Energy.IItemCapacitor;
import orionindustries.chemicalenergy.Utilities.Energy.ItemCapacitor;
import orionindustries.chemicalenergy.Utilities.Utilities;

import java.util.List;

public class ItemBatteryRack extends ItemCapacitor implements IItemCapacitor, IEnergyContainerItem
{
	private IIcon icon;
	private IIcon[] batteries;
	private IIcon blank;

	public static ItemBatteryRack Instance;

	public ItemBatteryRack()
	{
		super(0, 0, true);
		this.setMaxStackSize(1);
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
		Instance = this;
	}


	@Override
	public String getUnlocalizedName()
	{
		return "item." + Constants.ModID + ".BatteryRack";
	}

	@Override
	@SuppressWarnings({"unchecked", "raw"})
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4)
	{
		assert (stack.getItem() instanceof ItemBatteryRack);
		ItemBatteryRack batt = (ItemBatteryRack) stack.getItem();

		int num = 0;
		for(ItemStack stack1 : GetRackContents(stack))
		{
			if(stack1 != null)
				num++;
		}

		list.add(String.format("%d cell%s", num, num != 1 ? "s" : ""));
		if(num > 0)
		{
			list.add(Utilities.FormatNumber(batt.GetStoredEnergy(stack), false, true) + "RF" +
				" | " + Utilities.FormatNumber(batt.GetMaxEnergy(stack), true) + "RF");
		}
		else
		{
			list.add("Empty");
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		return this.getUnlocalizedName();
	}

	@Override
	public int getRenderPasses(int metadata)
	{
		return 7;
	}

	@Override
	public void registerIcons(IIconRegister iir)
	{
		this.icon = iir.registerIcon(Constants.ResourcePrefix + "Batteries/BatteryRack/BatteryRack");
		this.blank = iir.registerIcon(Constants.ResourcePrefix + "Batteries/BatteryRack/Blank");
		this.batteries = new IIcon[6];
		for(int i = 0; i < 6; i++)
			this.batteries[i] = iir.registerIcon(Constants.ResourcePrefix + "Batteries/BatteryRack/" + String.valueOf(i + 1));
	}

	@Override
	public IIcon getIcon(ItemStack stack,  int pass)
	{
		if(pass == 0)
			return this.icon;

		else
		{
			if(GetRackContents(stack)[pass - 1] != null)
				return this.batteries[pass - 1];

			else
				return this.blank;
		}
	}

	@Override
	public boolean requiresMultipleRenderPasses()
	{
		return true;
	}

	@Override
	public int getMaxItemUseDuration(ItemStack par1ItemStack)
	{
		return 1;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player)
	{
		if(!player.isSneaking())
		{
			player.openGui(ChemicalEnergy.Instance, Constants.GuiIDs.BatteryRack.getID(), player.worldObj,
				(int) player.posX, (int) player.posY, (int) player.posZ);
		}
		return stack;
	}

	public static ItemStack[] GetRackContents(ItemStack stack)
	{
		ItemStack[] stacks = new ItemStack[InventoryBatteryRack.InventorySize];

		if(stack.stackTagCompound == null)
			stack.stackTagCompound = new NBTTagCompound();

		NBTTagList items = stack.getTagCompound().getTagList("Items", net.minecraftforge.common.util.Constants.NBT.TAG_COMPOUND);

		for(int i = 0; i < items.tagCount(); ++i)
		{
			NBTTagCompound item = items.getCompoundTagAt(i);
			byte slot = item.getByte("Slot");

			if(slot >= 0 && slot < stacks.length)
			{
				stacks[slot] = ItemStack.loadItemStackFromNBT(item);
			}
		}

		return stacks;
	}

	@Override
	public int GetTransferRate(ItemStack stack)
	{
		ItemStack[] batteries = GetRackContents(stack);
		// slots 1, 2 and 3 are wired in series, so add the outputs together.
		int output = 0;
		for(ItemStack stack1 : batteries)
		{
			if(stack1 != null)
				output += GenericBattery.Instance.GetTransferRate(stack1);
		}

		return output;
	}

	@Override
	public int GetMaxEnergy(ItemStack stack)
	{
		ItemStack[] batteries = GetRackContents(stack);
		int cap = 0;
		for(ItemStack stack1 : batteries)
		{
			if(stack1 != null)
				cap += GenericBattery.Instance.GetMaxEnergy(stack1);
		}
		return cap;
	}

	@Override
	public int GetStoredEnergy(ItemStack stack)
	{
		ItemStack[] batteries = GetRackContents(stack);
		// slots 1, 2 and 3 are wired in series, so get the average capacity.
		int stored = 0;
		for(ItemStack stack1 : batteries)
		{
			if(stack != null)
				stored += GenericBattery.Instance.GetStoredEnergy(stack1);
		}

		return stored;
	}




	@Override
	public int StoreEnergy(ItemStack stack, int Energy)
	{
		ItemStack[] batteries = GetRackContents(stack);
		int num = 0;
		int received = 0;


		for(ItemStack stack1 : batteries)
			if(stack1 != null)
				num++;

		if(num == 0)
			return 0;

		int split = Energy / num;

		for(ItemStack stack1 : batteries)
			if(stack1 != null)
				received += GenericBattery.Instance.StoreEnergy(stack1, split);

		return received;
	}

	@Override
	public int ExtractEnergy(ItemStack stack, int Energy)
	{
		ItemStack[] batteries = GetRackContents(stack);
		int num = 0;
		int extracted = 0;

		for(ItemStack stack1 : batteries)
			if(stack1 != null)
				num++;

		if(num == 0)
			return 0;


		int split = Energy / num;

		for(ItemStack stack1 : batteries)
			if(stack1 != null)
				extracted += GenericBattery.Instance.ExtractEnergy(stack1, split);

		return extracted;
	}


	@Override
	public int receiveEnergy(ItemStack container, int maxReceive, boolean simulate)
	{
		return this.StoreEnergy(container, maxReceive);
	}

	@Override
	public int extractEnergy(ItemStack container, int maxExtract, boolean simulate)
	{
		return this.ExtractEnergy(container, maxExtract);
	}

	@Override
	public int getEnergyStored(ItemStack container)
	{
		return this.GetStoredEnergy(container);
	}

	@Override
	public int getMaxEnergyStored(ItemStack container)
	{
		return this.GetMaxEnergy(container);
	}
}
