// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Energy.Batteries.GenericBattery;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import orionindustries.chemicalenergy.ChemicalEnergy;
import orionindustries.chemicalenergy.Utilities.Energy.IItemCapacitor;
import orionindustries.chemicalenergy.Utilities.Energy.ItemCapacitor;
import orionindustries.chemicalenergy.Utilities.Utilities;

import java.util.List;

public abstract class GenericBattery extends ItemCapacitor implements IItemCapacitor
{
	public static GenericBattery Instance;
	public final boolean isWetCell;

	protected GenericBattery(int capacity, int transferrate, boolean Rechargable, boolean WetCell)
	{
		super(capacity, transferrate, Rechargable);
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
		this.setMaxStackSize(1);
		this.isWetCell = WetCell;
		Instance = this;
	}

	@Override
	public abstract String getUnlocalizedName();

	@Override
	public abstract String getUnlocalizedName(ItemStack stack);

	@Override
	@SuppressWarnings({"unchecked"})
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean par4)
	{
		assert (stack.getItem() instanceof GenericBattery);
		GenericBattery batt = (GenericBattery) stack.getItem();

		list.add(Utilities.FormatNumber(batt.GetStoredEnergy(stack), false, true) + "RF" +
			" | " + Utilities.FormatNumber(batt.GetMaxEnergy(stack), true) + "RF");
	}

	@Override
	public abstract void registerIcons(IIconRegister iir);


	public static boolean IsDryCell(ItemStack stack)
	{
		return stack.getItem() instanceof GenericBattery && !((GenericBattery) stack.getItem()).isWetCell;

	}
}
