// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.UtilityItems;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import orionindustries.chemicalenergy.ChemicalEnergy;

public abstract class AbstractItem extends Item
{
	public AbstractItem()
	{
		super();
		this.setCreativeTab(ChemicalEnergy.getCreativeTab());
	}

	@Override
	public abstract String getUnlocalizedName();

	@Override
	public abstract String getUnlocalizedName(ItemStack par1ItemStack);

	@Override
	public abstract void registerIcons(IIconRegister iir);
}
