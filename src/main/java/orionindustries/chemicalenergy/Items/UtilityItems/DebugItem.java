// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.UtilityItems;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import orionindustries.chemicalenergy.Utilities.CentralRegistry;
import orionindustries.chemicalenergy.Utilities.Constants;

public class DebugItem extends AbstractItem
{
	private IIcon icon;

	@Override
	public String getUnlocalizedName()
	{
		return "item." + Constants.ModID + ".DebugItem";
	}

	@Override
	public String getUnlocalizedName(ItemStack par1ItemStack)
	{
		return this.getUnlocalizedName();
	}

	@Override
	public void registerIcons(IIconRegister iir)
	{
		this.icon = iir.registerIcon(Constants.ResourcePrefix + "DebugItem");
	}

	@Override
	public IIcon getIconFromDamage(int par1)
	{
		return this.icon;
	}







	@Override
	public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player)
	{
		if(!player.isSneaking())
			CentralRegistry.RegisterCustomRecipes();

		return stack;
	}
}
