// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Materials;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import orionindustries.chemicalenergy.Items.UtilityItems.AbstractItem;
import orionindustries.chemicalenergy.Utilities.Constants;

import java.util.List;

public class ItemMaterial extends AbstractItem
{
	public static ItemMaterial Instance;
	IIcon[] icons;

	public ItemMaterial()
	{
		super();
		this.setHasSubtypes(true);
		Instance = this;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "item." + Constants.ModID + "." + "ItemMaterial";
	}

	@Override
	public void registerIcons(IIconRegister iir)
	{
		this.icons = new IIcon[Constants.Materials.values().length];

		for(int i = 0; i < this.icons.length; i++)
		{
			this.icons[i] = iir.registerIcon(Constants.ResourcePrefix + "Materials/Dusts/"
					+ Constants.Materials.values()[i].name());
		}
	}

	@Override
	@SuppressWarnings({ "raw", "unchecked" })
	public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean b)
	{
		list.add(Constants.Materials.values()[stack.getItemDamage()].GetDescription());
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return "item." + Constants.ModID + "." + Constants.Materials.values()[stack.getItemDamage()].name();
	}

	@Override
	public IIcon getIconFromDamage(int par1)
	{
		return this.icons[par1];
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for(int i = 0; i < Constants.Materials.values().length; i++)
		{
			list.add(new ItemStack(this, 1, i));
		}
	}
}
