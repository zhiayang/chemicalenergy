// Copyright (c) Forever, zhiayang@gmail.com | zhiayang@outlook.com
// Licensed under Creative Commons Attribution-ShareAlike 3.0 Unported.

package orionindustries.chemicalenergy.Items.Materials;

import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import orionindustries.chemicalenergy.Items.UtilityItems.AbstractItem;
import orionindustries.chemicalenergy.Utilities.Constants;

import java.util.List;

public class ItemMetalIngot extends AbstractItem
{
	private IIcon[] icons;

	public static ItemMetalIngot Instance;

	public ItemMetalIngot()
	{
		super();
		this.setHasSubtypes(true);
		Instance = this;
	}

	@Override
	public String getUnlocalizedName()
	{
		return "item." + Constants.ModID + ".ItemMetalIngot";
	}


	@Override
	public void registerIcons(IIconRegister iir)
	{
		this.icons = new IIcon[Constants.MetalIngots.values().length];

		for(int i = 0; i < this.icons.length; i++)
		{
			this.icons[i] = iir.registerIcon(Constants.ResourcePrefix + "Materials/Ingots/"
					+ Constants.MetalIngots.values()[i].name() + "Ingot");
		}
	}

	@Override
	public String getUnlocalizedName(ItemStack stack)
	{
		return "item." + Constants.ModID + "." + Constants.MetalIngots.values()[stack.getItemDamage()].name() + "Ingot";
	}

	@Override
	public IIcon getIconFromDamage(int par1)
	{
		return this.icons[par1];
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void getSubItems(Item item, CreativeTabs tab, List list)
	{
		for(int i = 0; i < Constants.MetalIngots.values().length; i++)
		{
			list.add(new ItemStack(this, 1, i));
		}
	}
}
